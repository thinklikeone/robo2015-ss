package thinklikeone.gui;

import java.util.ArrayList;
import java.util.List;

/**
 * Container for more controls (or containers)
 *
 * @author Jakub Vaněk {@literal <vanek.jakub4@seznam.cz>}
 */
public abstract class Container extends Control {
	protected List<Control> controls;
	protected Control focused = null;

	public Container() {
		super();
		controls = new ArrayList<>();
	}

	public Container(int x, int y, int width, int height, Container parent) {
		super(x, y, width, height, Platform.getPlatform().getRenderer(), parent);
		controls = new ArrayList<>();
	}

	public Container(List<Control> controls) {
		super();
		this.controls = controls;
	}

	public Container(List<Control> controls, int x, int y, int width, int height, Container parent) {
		super(x, y, width, height, parent);
		this.controls = controls;
	}

	public void addControl(Control control) {
		controls.add(control);
		setFocus(control);
	}

	public void clearControls() {
		controls.clear();
		focused = null;
	}

	public void deleteControl(Control control) {
		controls.remove(control);
	}

	public List<Control> getChildren() {
		return controls;
	}

	public void setFocus(Control focus) {
		if (controls.contains(focus)) // prevent input stealing
			focused = focus;
	}

	public Control getFocused() {
		return focused;
	}

	public int getAbsoluteX() {
		int x = this.x;
		Container level = this;
		while ((level = level.getParent()) != null) {
			x += level.getX();
		}
		return x;
	}

	public int getAbsoluteY() {
		int y = this.y;
		Container level = this;
		while ((level = level.getParent()) != null) {
			y += level.getY();
		}
		return y;
	}
}
