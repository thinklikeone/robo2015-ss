package thinklikeone.gui;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Swing JPanel Keyboard I/O
 *
 * @author Jakub Vaněk {@literal <vanek.jakub4@seznam.cz>}
 */
public class SwingKeyboard extends Keyboard implements KeyListener {
	private KeyCode lastKeyCode;
	private char lastChar;
	private boolean physical;

	public SwingKeyboard(Component input) {
		this(input, false);
	}

	public SwingKeyboard(Component input, boolean physical) {
		super();
		input.addKeyListener(this);
		this.physical = physical;
	}

	@Override
	public KeyCode getKey() {
		return lastKeyCode;
	}

	@Override
	public char getChar() {
		return lastChar;
	}

	@Override
	public boolean getPhysical() {
		return physical;
	}

	@Override
	public void setPhysical(boolean physical) {
		this.physical = physical;
	}

	@Override
	public void addToStack(InputStack listener) {
		outputs.add(listener);
		ids.add(listener.generateID());
	}

	@Override
	public void activate() {

	}

	@Override
	public void deactivate() {

	}

	@Override
	public int getID(InputStack stack) {
		for (int i = 0; i < outputs.size(); i++) {
			if (outputs.get(i) == stack)
				return ids.get(i);
		}
		return -1;
	}

	@Override
	public void keyTyped(KeyEvent e) {
	}

	@Override
	public void keyPressed(KeyEvent e) {
		int code = e.getKeyCode();
		lastKeyCode = parseKey(code);
		lastChar = e.getKeyChar();
		pushKeyboardEvent(lastKeyCode, lastChar, KeyboardEvent.Type.PRESS);
	}

	@Override
	public void keyReleased(KeyEvent e) {
		int code = e.getKeyCode();
		lastKeyCode = parseKey(code);
		lastChar = e.getKeyChar();
		pushKeyboardEvent(lastKeyCode, lastChar, KeyboardEvent.Type.RELEASE);
	}

	public KeyCode parseKey(int code) {
		if (physical) {
			switch (code) {
				case KeyEvent.VK_ESCAPE:
					return KeyCode.PHYS_ESCAPE;
				case KeyEvent.VK_ENTER:
					return KeyCode.PHYS_ENTER;
				case KeyEvent.VK_UP:
					return KeyCode.PHYS_UP;
				case KeyEvent.VK_DOWN:
					return KeyCode.PHYS_DOWN;
				case KeyEvent.VK_LEFT:
					return KeyCode.PHYS_LEFT;
				case KeyEvent.VK_RIGHT:
					return KeyCode.PHYS_RIGHT;
			}
		}
		switch (code) {
			case KeyEvent.VK_A:
				return KeyCode.A;
			case KeyEvent.VK_B:
				return KeyCode.B;
			case KeyEvent.VK_C:
				return KeyCode.C;
			case KeyEvent.VK_D:
				return KeyCode.D;
			case KeyEvent.VK_E:
				return KeyCode.E;
			case KeyEvent.VK_F:
				return KeyCode.F;
			case KeyEvent.VK_G:
				return KeyCode.G;
			case KeyEvent.VK_H:
				return KeyCode.H;
			case KeyEvent.VK_I:
				return KeyCode.I;
			case KeyEvent.VK_J:
				return KeyCode.J;
			case KeyEvent.VK_K:
				return KeyCode.K;
			case KeyEvent.VK_L:
				return KeyCode.L;
			case KeyEvent.VK_M:
				return KeyCode.M;
			case KeyEvent.VK_N:
				return KeyCode.N;
			case KeyEvent.VK_O:
				return KeyCode.O;
			case KeyEvent.VK_P:
				return KeyCode.P;
			case KeyEvent.VK_Q:
				return KeyCode.Q;
			case KeyEvent.VK_R:
				return KeyCode.R;
			case KeyEvent.VK_S:
				return KeyCode.S;
			case KeyEvent.VK_T:
				return KeyCode.T;
			case KeyEvent.VK_U:
				return KeyCode.U;
			case KeyEvent.VK_V:
				return KeyCode.V;
			case KeyEvent.VK_W:
				return KeyCode.W;
			case KeyEvent.VK_X:
				return KeyCode.X;
			case KeyEvent.VK_Y:
				return KeyCode.Y;
			case KeyEvent.VK_Z:
				return KeyCode.Z;

			case KeyEvent.VK_0:
				return KeyCode.N0;
			case KeyEvent.VK_1:
				return KeyCode.N1;
			case KeyEvent.VK_2:
				return KeyCode.N2;
			case KeyEvent.VK_3:
				return KeyCode.N3;
			case KeyEvent.VK_4:
				return KeyCode.N4;
			case KeyEvent.VK_5:
				return KeyCode.N5;
			case KeyEvent.VK_6:
				return KeyCode.N6;
			case KeyEvent.VK_7:
				return KeyCode.N7;
			case KeyEvent.VK_8:
				return KeyCode.N8;
			case KeyEvent.VK_9:
				return KeyCode.N9;

			case KeyEvent.VK_F1:
				return KeyCode.SPECIAL_1;
			case KeyEvent.VK_F2:
				return KeyCode.SPECIAL_2;
			case KeyEvent.VK_F3:
				return KeyCode.SPECIAL_3;
			case KeyEvent.VK_F4:
				return KeyCode.SPECIAL_4;
			case KeyEvent.VK_F5:
				return KeyCode.SPECIAL_5;
			case KeyEvent.VK_F6:
				return KeyCode.SPECIAL_6;
			case KeyEvent.VK_F7:
				return KeyCode.SPECIAL_7;
			case KeyEvent.VK_F8:
				return KeyCode.SPECIAL_8;
			case KeyEvent.VK_F9:
				return KeyCode.SPECIAL_9;
			case KeyEvent.VK_F10:
				return KeyCode.SPECIAL_10;
			case KeyEvent.VK_F11:
				return KeyCode.SPECIAL_11;
			case KeyEvent.VK_F12:
				return KeyCode.SPECIAL_12;

			case KeyEvent.VK_EXCLAMATION_MARK:
				return KeyCode.EXCLAMATION;
			case KeyEvent.VK_QUOTEDBL:
				return KeyCode.DOUBLEQUOTE;
			case KeyEvent.VK_NUMBER_SIGN:
				return KeyCode.HASH;
			case KeyEvent.VK_DOLLAR:
				return KeyCode.DOLLAR;
			// todo no percent
			case KeyEvent.VK_AMPERSAND:
				return KeyCode.AMPERSAND;
			case KeyEvent.VK_BACK_QUOTE:
				return KeyCode.BACKQUOTE;
			case KeyEvent.VK_LEFT_PARENTHESIS:
				return KeyCode.LPAREN;
			case KeyEvent.VK_RIGHT_PARENTHESIS:
				return KeyCode.RPAREN;
			case KeyEvent.VK_ASTERISK:
				return KeyCode.ASTERISK;
			case KeyEvent.VK_PLUS:
				return KeyCode.PLUS;
			case KeyEvent.VK_MINUS:
				return KeyCode.MINUS;
			case KeyEvent.VK_COMMA:
				return KeyCode.COMMA;
			case KeyEvent.VK_PERIOD:
				return KeyCode.DOT;
			case KeyEvent.VK_SLASH:
				return KeyCode.SLASH;
			case KeyEvent.VK_COLON:
				return KeyCode.COLON;
			case KeyEvent.VK_SEMICOLON:
				return KeyCode.SEMICOLON;
			case KeyEvent.VK_LESS:
				return KeyCode.LESSER;
			case KeyEvent.VK_EQUALS:
				return KeyCode.EQUAL;
			case KeyEvent.VK_GREATER:
				return KeyCode.GREATER;
			// todo no question mark
			case KeyEvent.VK_AT:
				return KeyCode.AT;
			case KeyEvent.VK_OPEN_BRACKET:
				return KeyCode.LSQUAREBRACKET;
			case KeyEvent.VK_CLOSE_BRACKET:
				return KeyCode.RSQUAREBRACKET;
			case KeyEvent.VK_BACK_SLASH:
				return KeyCode.BACKSLASH;
			case KeyEvent.VK_DEAD_GRAVE:
				return KeyCode.GRAVE;
			case KeyEvent.VK_CIRCUMFLEX:
				return KeyCode.EXPONENT;
			case KeyEvent.VK_UNDERSCORE:
				return KeyCode.UNDERSCORE;
			case KeyEvent.VK_QUOTE:
				return KeyCode.QUOTE;
			case KeyEvent.VK_BRACELEFT:
				return KeyCode.LCURLYBRACKET;
			case KeyEvent.VK_BRACERIGHT:
				return KeyCode.LCURLYBRACKET;
			// todo no pipe
			case KeyEvent.VK_DEAD_TILDE:
				return KeyCode.TILDE;
			case KeyEvent.VK_SPACE:
				return KeyCode.SPACE;
			case KeyEvent.VK_ENTER:
				// TODO handle this correctly
				//	return KeyCode.ENTER;
				return KeyCode.ACCEPT;
			case KeyEvent.VK_SHIFT:
				return KeyCode.SHIFT;
			case KeyEvent.VK_TAB:
				return KeyCode.SYMBOL;
			case KeyEvent.VK_INSERT:
				return KeyCode.ACCEPT;
			case KeyEvent.VK_ESCAPE:
				return KeyCode.EXIT;
			case KeyEvent.VK_BACK_SPACE:
				return KeyCode.BACKSPACE;
			case KeyEvent.VK_LEFT:
				return KeyCode.LEFT;
			case KeyEvent.VK_RIGHT:
				return KeyCode.RIGHT;
		}
		return KeyCode.UNKNOWN;
	}
}
