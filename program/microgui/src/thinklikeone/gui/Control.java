package thinklikeone.gui;

import lejos.hardware.lcd.GraphicsLCD;

/**
 * Control base class
 *
 * @author Jakub Vaněk {@literal <vanek.jakub4@seznam.cz>}
 */
public abstract class Control implements InputListener, Drawable {
	protected Container parent;
	protected int x;
	protected int y;
	protected int width;
	protected Renderer render;
	protected int height;

	public Control() {
		this(0, 0, 100, 100, Platform.getPlatform().getRenderer(), null);
	}

	public Control(int x, int y, int width, int height, Renderer render, Container parent) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.render = render;
		this.parent = parent;
	}

	public Control(int x, int y, int width, int height, Container parent) {
		this(x, y, width, height, Platform.getPlatform().getRenderer(), parent);
	}

	@Override
	public int getHeight() {
		return height;
	}

	@Override
	public void setHeight(int height) {
		this.height = height;
	}

	@Override
	public int getWidth() {
		return width;
	}

	@Override
	public void setWidth(int width) {
		this.width = width;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public Container getParent() {
		return parent;
	}

	protected void clear() {
		GraphicsLCD lcd = render.getLCD();
		lcd.setColor(GraphicsLCD.WHITE);
		lcd.fillRect(x, y, width, height);
		lcd.setColor(GraphicsLCD.BLACK);
	}
}
