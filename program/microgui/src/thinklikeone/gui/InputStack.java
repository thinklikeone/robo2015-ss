package thinklikeone.gui;

/**
 * Input event proxy
 *
 * @author Jakub Vaněk {@literal <vanek.jakub4@seznam.cz>}
 */
public interface InputStack {
	/**
	 * Add event to queue.
	 *
	 * @param event Event to add.
	 */
	void putEvent(Event event);

	/**
	 * Add input hook to a input provider.
	 *
	 * @param provider Input provider to use.
	 */
	void addInputProvider(InputProvider provider);

	/**
	 * Add input hook to a input listener.
	 *
	 * @param listener Input listener to use.
	 */
	void addInputListener(InputListener listener);

	/**
	 * Generate new event source ID. This function is intended for preventing ID collisions.
	 *
	 * @return Source ID.
	 */
	int generateID();
}
