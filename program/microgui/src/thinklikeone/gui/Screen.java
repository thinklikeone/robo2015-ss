package thinklikeone.gui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Screen abstraction
 *
 * @author Jakub Vaněk {@literal <vanek.jakub4@seznam.cz>}
 */
public class Screen implements InputListener, Drawable {
	private static final LayerZComparer zComparator = new LayerZComparer();
	private Renderer render;
	private int width;
	private int height;
	private List<Layer> layers;
	private Drawable header;
	private Drawable footer;

	public Screen(Renderer output) {
		this.render = output;
		this.layers = new ArrayList<>();
	}

	public void setHeader(Drawable drawable) {
		header = drawable;
	}

	public void setFooter(Drawable drawable) {
		footer = drawable;
	}

	public int getHeaderBottomY() {
		return header.getHeight();
	}

	public int getFooterTopY() {
		return height - footer.getHeight();
	}

	public Layer getLayer(int x, int y, float z, int width, int height) {
		Layer result = getLayerAt(z);
		if (result != null)
			return result;
		int checkedY = header == null ? y : y + header.getHeight();
		result = new Layer(this, x, checkedY, z, width, height);
		layers.add(result);
		Collections.sort(layers, zComparator);
		return result;
	}

	public void deleteLayer(Layer layer) {
		layers.remove(layer);
		Collections.sort(layers, zComparator);
	}

	public void deleteLayer(float z) {
		for (int i = 0; i < layers.size(); i++) {
			if (layers.get(i).z == z) {
				layers.remove(i);
				Collections.sort(layers, zComparator);
				break;
			}
		}
	}

	public Layer getLayerAt(float z) {
		for (Layer layer : layers) {
			if (layer.z == z) {
				return layer;
			}
		}
		return null;
	}

	@Override
	public void eventHandler(Event event) {
		for (Layer layer : layers) {
			if (layer.ignoreInput)
				continue;
			layer.getContainer().eventHandler(event);
			break;
		}
	}

	@Override
	public void draw(int x, int y) {
		if (header != null)
			header.draw(0, 0);
		if (footer != null)
			footer.draw(0, height - footer.getHeight());
		int count = layers.size() > 0 ? 1 : 0;
		if (count > 0) {
			for (int i = 0; i < layers.size(); i++) {
				if (layers.get(i).isTransparent()) {
					count++;
				} else {
					break;
				}
			}
			if (count > layers.size())
				count = layers.size();
			for (int i = count - 1; i >= 0; i--) {
				Layer layer = layers.get(i);
				Container container = layer.getContainer();
				container.draw(0, 0);
			}
		}
		if (Platform.getPlatform().getKeyboard() instanceof Drawable) {
			Drawable k = (Drawable) Platform.getPlatform().getKeyboard();
			k.draw(0, 0);
		}
	}

	@Override
	public int getWidth() {
		return width;
	}

	@Override
	public void setWidth(int w) {
		width = w;
	}

	@Override
	public int getHeight() {
		return height;
	}

	@Override
	public void setHeight(int h) {
		height = h;
	}

	public static class Layer {
		private boolean transparent = false;
		private boolean ignoreInput = false;
		private Container cont;
		private Screen parent;
		private int x;
		private int y;
		private float z;
		private int width;
		private int height;

		public Layer(Screen parent, int x, int y, float z, int width, int height) {
			this(parent, false, false, x, y, z, width, height);
		}

		public Layer(Screen parent, boolean transparentBackground, boolean inputIgnore, int x, int y, float z, int width, int height) {
			this.parent = parent;
			this.transparent = transparentBackground;
			this.cont = new AbsoluteContainer(0, 0, this.width, this.height, new DummyContainer());
			setX(x);
			setY(y);
			setZ(z);
			setWidth(width);
			setHeight(height);
			setInputIgnore(inputIgnore);
		}

		public int getHeight() {
			return height;
		}

		public void setHeight(int height) {
			this.height = height;
		}

		public int getX() {
			return x;
		}

		public void setX(int x) {
			this.x = x;
		}

		public int getY() {
			return y;
		}

		public void setY(int y) {
			this.y = y;
		}

		public float getZ() {
			return z;
		}

		public void setZ(float z) {
			this.z = z;
		}

		public int getWidth() {
			return width;
		}

		public void setWidth(int width) {
			this.width = width;
		}

		public boolean isTransparent() {
			return transparent;
		}

		public void setTransparent(boolean transparent) {
			this.transparent = transparent;
		}

		public Container getContainer() {
			return cont;
		}

		public void setContainer(Container cont) {
			this.cont = cont;
		}

		@Override
		public boolean equals(Object layer) {
			return layer instanceof Layer && this.z == ((Layer) layer).z;
		}

		public Screen getParent() {
			return parent;
		}

		public boolean getInputIgnore() {
			return ignoreInput;
		}

		public void setInputIgnore(boolean ignoreInput) {
			this.ignoreInput = ignoreInput;
		}

		private class DummyContainer extends Container {
			public DummyContainer() {
				this.parent = null;
			}

			@Override
			public int getX() {
				return Layer.this.x;
			}

			@Override
			public int getY() {
				return Layer.this.y;
			}

			@Override
			public int getWidth() {
				return Layer.this.width;
			}

			@Override
			public int getHeight() {
				return Layer.this.height;
			}

			@Override
			public void draw(int x, int y) {
			}

			@Override
			public void eventHandler(Event event) {
			}
		}
	}

	/**
	 * Comparer for sorting starting with layers with greatest Z values to those with smaller ones.
	 * TL;DR: comparer for reverse float sort
	 */
	private static class LayerZComparer implements Comparator<Layer> {

		@Override
		public int compare(Layer o1, Layer o2) {
			return Float.compare(o2.getZ(), o1.getZ());
		}
	}
}
