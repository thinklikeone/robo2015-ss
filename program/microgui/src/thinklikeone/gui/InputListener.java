package thinklikeone.gui;

import java.util.EventListener;

/**
 * Input event listener
 *
 * @author Jakub Vaněk {@literal <vanek.jakub4@seznam.cz>}
 */
public interface InputListener extends EventListener {
	/**
	 * Event dispatch.
	 *
	 * @param event Event info.
	 */
	void eventHandler(Event event);
}
