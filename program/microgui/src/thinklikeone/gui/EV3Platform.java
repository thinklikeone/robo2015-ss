package thinklikeone.gui;

import lejos.hardware.Brick;
import lejos.hardware.BrickFinder;
import lejos.hardware.Button;
import lejos.hardware.lcd.GraphicsLCD;

import java.util.Timer;
import java.util.TimerTask;

/**
 * EV3 platform implementation.
 *
 * @author Jakub Vaněk {@literal <vanek.jakub4@seznam.cz>}
 */
public class EV3Platform extends Platform {
	private EV3Renderer rendererImpl;
	private EV3Keyboard kbd;
	private GraphicsLCD lcd;

	public EV3Platform() {
		this(BrickFinder.getLocal());
	}

	public EV3Platform(Brick brick) {
		lcd = brick.getGraphicsLCD();
		rendererImpl = new EV3Renderer();
		kbd = EV3Keyboard.getInstance();
		try {
			kbd.initialize(rendererImpl, true);
			this.addInputProvider(kbd);
			ButtonThread buttons = new ButtonThread(kbd);
			buttons.start();

		} catch (InstantiationException e) {
		}
	}

	@Override
	public Renderer getRenderer() {
		return rendererImpl;
	}

	public Keyboard getKeyboard() {
		return kbd;
	}

	private static class ButtonThread extends Thread {
		private static final int FIRST_WAIT = 400;
		private static final int WAIT = 200;
		private volatile boolean exit = false;
		private lejos.hardware.KeyListener target;
		private Timer escapeTimer;
		private Timer enterTimer;
		private Timer upTimer;
		private Timer downTimer;
		private Timer leftTimer;
		private Timer rightTimer;

		public ButtonThread(EV3Keyboard listener) {
			this.target = listener;
			this.setDaemon(true);
			this.setName("EV3 Button thread");
			this.setPriority(Thread.MAX_PRIORITY);
			Thread exit = new Thread(new Runnable() {
				@Override
				public void run() {
					exit();
				}
			});
			Runtime.getRuntime().addShutdownHook(exit);
		}

		private void exit() {
			exit = true;
		}

		@Override
		public void run() {
			while (!exit) {
				int data = Button.waitForAnyEvent();
				int press = data & Button.ID_ALL;
				int release = (data >>> 8) & Button.ID_ALL;
				if ((press & Button.ID_RIGHT) != 0) {
					target.keyPressed(Button.RIGHT);
					cancelCountdown(rightTimer);
					rightTimer = new Timer(true);
					rightTimer.scheduleAtFixedRate(new TimerTask() {
						@Override
						public void run() {
							target.keyPressed(Button.RIGHT);
							target.keyReleased(Button.RIGHT);
						}
					}, FIRST_WAIT, WAIT);
				}
				if ((press & Button.ID_LEFT) != 0) {
					target.keyPressed(Button.LEFT);
					cancelCountdown(leftTimer);
					leftTimer = new Timer(true);
					leftTimer.scheduleAtFixedRate(new TimerTask() {
						@Override
						public void run() {
							target.keyPressed(Button.LEFT);
							target.keyReleased(Button.LEFT);
						}
					}, FIRST_WAIT, WAIT);
				}
				if ((press & Button.ID_UP) != 0) {
					target.keyPressed(Button.UP);
					cancelCountdown(upTimer);
					upTimer = new Timer(true);
					upTimer.scheduleAtFixedRate(new TimerTask() {
						@Override
						public void run() {
							target.keyPressed(Button.UP);
							target.keyReleased(Button.UP);
						}
					}, FIRST_WAIT, WAIT);
				}
				if ((press & Button.ID_DOWN) != 0) {
					target.keyPressed(Button.DOWN);
					cancelCountdown(downTimer);
					downTimer = new Timer(true);
					downTimer.scheduleAtFixedRate(new TimerTask() {
						@Override
						public void run() {
							target.keyPressed(Button.DOWN);
							target.keyReleased(Button.DOWN);
						}
					}, FIRST_WAIT, WAIT);
				}
				if ((press & Button.ID_ENTER) != 0) {
					target.keyPressed(Button.ENTER);
					cancelCountdown(enterTimer);
					enterTimer = new Timer(true);
					enterTimer.scheduleAtFixedRate(new TimerTask() {
						@Override
						public void run() {
							target.keyPressed(Button.ENTER);
							target.keyReleased(Button.ENTER);
						}
					}, FIRST_WAIT, WAIT);
				}
				if ((press & Button.ID_ESCAPE) != 0) {
					target.keyPressed(Button.ESCAPE);
					cancelCountdown(escapeTimer);
					escapeTimer = new Timer(true);
					escapeTimer.scheduleAtFixedRate(new TimerTask() {
						@Override
						public void run() {
							target.keyPressed(Button.ESCAPE);
							target.keyReleased(Button.ESCAPE);
						}
					}, FIRST_WAIT, WAIT);
				}


				if ((release & Button.ID_RIGHT) != 0) {
					target.keyReleased(Button.RIGHT);
					cancelCountdown(rightTimer);
				}
				if ((release & Button.ID_LEFT) != 0) {
					target.keyReleased(Button.LEFT);
					cancelCountdown(leftTimer);
				}
				if ((release & Button.ID_UP) != 0) {
					target.keyReleased(Button.UP);
					cancelCountdown(upTimer);
				}
				if ((release & Button.ID_DOWN) != 0) {
					target.keyReleased(Button.DOWN);
					cancelCountdown(downTimer);
				}
				if ((release & Button.ID_ENTER) != 0) {
					target.keyReleased(Button.ENTER);
					cancelCountdown(enterTimer);
				}
				if ((release & Button.ID_ESCAPE) != 0) {
					target.keyReleased(Button.ESCAPE);
					cancelCountdown(escapeTimer);
				}
			}
		}

		private void cancelCountdown(Timer t) {
			try {
				t.cancel();
			} catch (Exception e) {
			}
		}
	}

	private class EV3Renderer extends Renderer {
		private EV3Renderer() {
		}

		@Override
		public GraphicsLCD getLCD() {
			return lcd;
		}
	}
}