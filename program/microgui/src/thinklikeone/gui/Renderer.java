package thinklikeone.gui;

import lejos.hardware.lcd.GraphicsLCD;

/**
 * Renderer interface
 *
 * @author Jakub Vaněk {@literal <vanek.jakub4@seznam.cz>}
 */
public abstract class Renderer {
	/**
	 * Get renderer implementation.
	 *
	 * @return GraphicsLCD implementation.
	 */
	public abstract GraphicsLCD getLCD();
}
