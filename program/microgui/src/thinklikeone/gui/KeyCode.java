package thinklikeone.gui;

/**
 * Keycode enumeration.
 *
 * @author Jakub Vaněk {@literal <vanek.jakub4@seznam.cz>}
 */
public enum KeyCode {
	A('a'), B('b'), C('c'), D('d'), E('e'), F('f'), G('g'), H('h'),
	I('i'), J('j'), K('k'), L('l'), M('m'), N('n'), O('o'), P('p'),
	Q('q'), R('r'), S('s'), T('t'), U('u'), V('v'), W('w'), X('x'), Y('y'), Z('z'),
	N1('1'), N2('2'), N3('3'), N4('4'), N5('5'), N6('6'), N7('7'), N8('8'), N9('9'), N0('0'),
	EXCLAMATION('!'), DOUBLEQUOTE('\"'), HASH('#'), DOLLAR('$'), PERCENT('%'),
	AMPERSAND('&'), BACKQUOTE('`'), RPAREN('('), LPAREN(')'), ASTERISK('*'),
	PLUS('+'), MINUS('-'), COMMA(','), DOT('.'), SLASH('/'), COLON(':'), SEMICOLON(';'),
	LESSER('<'), EQUAL('='), GREATER('>'), QUESTION('?'), AT('@'),
	RSQUAREBRACKET('['), LSQUAREBRACKET(']'), BACKSLASH('\\'),
	GRAVE('`'), EXPONENT('^'), UNDERSCORE('_'), QUOTE('\''),
	LCURLYBRACKET('{'), RCURLYBRACKER('}'), PIPE('|'), TILDE('~'), SPACE(' '),
	ENTER('\n'), ACCEPT((char) 0), SYMBOL((char) 0), SHIFT((char) 0), EXIT((char) 0), UNKNOWN((char) 0),
	BACKSPACE((char) 0), RETURN((char) 0), LEFT((char) 0), RIGHT((char) 0),
	SPECIAL_1((char) 0), SPECIAL_2((char) 0), SPECIAL_3((char) 0),
	SPECIAL_4((char) 0), SPECIAL_5((char) 0), SPECIAL_6((char) 0),
	SPECIAL_7((char) 0), SPECIAL_8((char) 0), SPECIAL_9((char) 0),
	SPECIAL_10((char) 0), SPECIAL_11((char) 0), SPECIAL_12((char) 0),
	PHYS_LEFT((char) 0), PHYS_RIGHT((char) 0), PHYS_UP((char) 0),
	PHYS_DOWN((char) 0), PHYS_ENTER((char) 0), PHYS_ESCAPE((char) 0);
	char letter;

	KeyCode(char letter) {
		this.letter = letter;
	}

	/**
	 * Get character of this keycode.
	 *
	 * @return Predefined character.
	 */
	public char getChar() {
		return letter;
	}

	/**
	 * Get lowercase character of this keycode.
	 *
	 * @return Lowercase form of predefined character.
	 */
	public char toLowerCase() {
		return Character.toLowerCase(letter);
	}

	/**
	 * Get uppercase character of this keycode.
	 *
	 * @return Uppercase form of predefined character.
	 */
	public char toUpperCase() {
		return Character.toUpperCase(letter);
	}

	/**
	 * Check if this keycode is special.
	 *
	 * @return True if letter of this keycode is NUL.
	 */
	public boolean isSpecial() {
		return letter == 0;
	}
}