package thinklikeone.gui;

import lejos.hardware.Brick;
import lejos.hardware.BrickFinder;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Platform abstraction.
 *
 * @author Jakub Vaněk {@literal <vanek.jakub4@seznam.cz>}
 */
public abstract class Platform implements InputStack {
	private static Platform platform;

	static {
		try {
			setEV3Platform();
		} catch (Error e) {
			platform = null;
		}
	}

	protected int idCounter = 0;
	protected Screen screenInstance = null;
	protected List<InputListener> listenerList;
	protected BlockingQueue<Event> events;
	protected boolean immediate = false;

	protected Platform() {
		listenerList = new ArrayList<>();
		events = new LinkedBlockingQueue<>();
	}

	public static void setEV3Platform() throws Error {
		Brick brick = BrickFinder.getDefault();
		platform = new EV3Platform(brick);
	}

	public static Platform getPlatform() {
		return platform;
	}

	public static void setPlatform(Platform platform) {
		Platform.platform = platform;
	}

	@Override
	public int generateID() {
		return idCounter++; // *postfix*
	}

	public abstract Renderer getRenderer();

	public boolean getImmediate() {
		return immediate;
	}

	public void setImmediate(boolean value) {
		immediate = value;
	}

	public void addInputListener(InputListener listener) {
		listenerList.add(listener);
	}

	public void addInputProvider(InputProvider provider) {
		provider.addToStack(this);
	}

	public abstract Keyboard getKeyboard();

	public void putEvent(Event event) {
		try {
			events.put(event);
			if (immediate) {
				processEvents();
			}
		} catch (InterruptedException e) {
		}
	}

	public Screen createDefaultScreen() {
		if (screenInstance == null) {
			screenInstance = new Screen(getRenderer());
			this.addInputListener(screenInstance);
		}
		return screenInstance;
	}

	public void processEvents() {
		Event event;
		while ((event = events.poll()) != null) {
			for (InputListener listener : listenerList) {
				listener.eventHandler(event);
			}
		}
	}
}