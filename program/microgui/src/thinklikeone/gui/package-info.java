/**
 * ThinkLikeOne GUI system
 *
 * @version 0.1
 * @author Jakub Vaněk {@literal <vanek.jakub4@seznam.cz>}
 */
package thinklikeone.gui;