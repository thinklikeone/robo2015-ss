package thinklikeone.gui;

/**
 * Event structure
 *
 * @author Jakub Vaněk {@literal <vanek.jakub4@seznam.cz>}
 */
public abstract class Event {
	private int source;

	/**
	 * Create new event.
	 *
	 * @param source Event source ID.
	 */
	public Event(int source) {
		this.source = source;
	}

	/**
	 * Get source ID.
	 *
	 * @return Unique source ID.
	 */
	public int getSourceId() {
		return source;
	}
}
