package thinklikeone.setup;

/**
 * Battery interface
 *
 * @author Jakub Vaněk {@literal <vanek.jakub4@seznam.cz>}
 */
public interface Battery {
	float getVoltage();
}
