package thinklikeone.setup;

import lejos.hardware.lcd.GraphicsLCD;
import thinklikeone.gui.Event;
import thinklikeone.gui.KeyCode;
import thinklikeone.gui.KeyboardEvent;
import thinklikeone.gui.Platform;

/**
 * Dialog for entering integer numbers
 *
 * @author Jakub Vaněk {@literal <vanek.jakub4@seznam.cz>}
 */
public class IntEntry extends Entry<Integer> {
	private static IntEntry instance;
	private int value = 0;
	private boolean switched = false;
	private int original = 0;

	public IntEntry(GraphicsLCD surface) {
		this.lcd = surface;
	}

	@Override
	public void open(Integer value, String name) {
		this.value = value; // remove reference
		this.original = value;
		this.name = name;
		this.closed = false;
		this.switched = false;
		Platform.getPlatform().getKeyboard().setPhysical(true);
		Platform.getPlatform().getKeyboard().activate();
	}

	public boolean isSwitched() {
		return switched;
	}

	@Override
	public Integer getValue() {
		return value;
	}

	@Override
	public void setValue(Integer value) {
		this.value = value;
		this.original = value;
	}

	@Override
	public boolean valueChanged() {
		return original != value;
	}

	@Override
	public void draw(int x, int y) {
		drawName(x, y);
		lcd.drawRect(1 + x, Main.SMALL_FONT.height + 3 + y, lcd.getWidth() - 3, Main.NORMAL_FONT.height + 4);
		String intVal = Integer.toString(value);
		lcd.setFont(Main.NORMAL_FONT);
		lcd.drawString(intVal, lcd.getWidth() - 4 + x, Main.SMALL_FONT.height + 6 + y, TOP_RIGHT);
	}

	@Override
	public void eventHandler(Event event) {
		try {
			if (event instanceof KeyboardEvent) {
				KeyboardEvent<KeyCode> kbdEvent = (KeyboardEvent<KeyCode>) event;
				if (kbdEvent.getType() == KeyboardEvent.Type.PRESS) {
					switch (kbdEvent.getKey()) {
						case PHYS_UP:
							value++;
							break;
						case PHYS_DOWN:
							value--;
							break;
						case PHYS_RIGHT:
							switched = true;
							closed = true;
							break;
						case PHYS_ENTER:
							closed = true;
							Platform.getPlatform().getKeyboard().deactivate();
							break;
						case PHYS_ESCAPE:
							value = original;
							closed = true;
							Platform.getPlatform().getKeyboard().deactivate();
							break;
					}
				}
			}
		} catch (ClassCastException e) {
			System.err.println("BUG! " + e.getMessage());
		}
	}
}