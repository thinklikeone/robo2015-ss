package thinklikeone.setup;

import lejos.hardware.lcd.Image;
import thinklikeone.gui.EV3Keyboard;
import thinklikeone.gui.KeyCode;

/**
 * Number keyboard layout
 *
 * @author Jakub Vaněk {@literal <vanek.jakub4@seznam.cz>}
 */
public class NumberKeyboardLayout implements EV3Keyboard.KeyboardLayout {
	private static final Image BACKSPACE_IMG = new Image(10, 16, new byte[]
			{(byte) 0x00, (byte) 0x00, (byte) 0x08, (byte) 0x00, (byte) 0x04,
					(byte) 0x00, (byte) 0xfe, (byte) 0x01, (byte) 0x04, (byte) 0x00,
					(byte) 0x08, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00,
					(byte) 0x00, (byte) 0x84, (byte) 0x00, (byte) 0x48, (byte) 0x00,
					(byte) 0x30, (byte) 0x00, (byte) 0x30, (byte) 0x00, (byte) 0x48,
					(byte) 0x00, (byte) 0x84, (byte) 0x00, (byte) 0x00, (byte) 0x00,
					(byte) 0x00, (byte) 0x00,});
	private static final Image ACCEPT_IMG = new Image(10, 16, new byte[]
			{(byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00,
					(byte) 0x00, (byte) 0x00, (byte) 0x01, (byte) 0x00, (byte) 0x01,
					(byte) 0x80, (byte) 0x00, (byte) 0x80, (byte) 0x00, (byte) 0x40,
					(byte) 0x00, (byte) 0x40, (byte) 0x00, (byte) 0x22, (byte) 0x00,
					(byte) 0x24, (byte) 0x00, (byte) 0x18, (byte) 0x00, (byte) 0x10,
					(byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00,
					(byte) 0x00, (byte) 0x00,});
	private static final Image DECLINE_IMG = new Image(10, 16, new byte[]
			{(byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x02,
					(byte) 0x01, (byte) 0x84, (byte) 0x00, (byte) 0x84, (byte) 0x00,
					(byte) 0x48, (byte) 0x00, (byte) 0x30, (byte) 0x00, (byte) 0x30,
					(byte) 0x00, (byte) 0x30, (byte) 0x00, (byte) 0x30, (byte) 0x00,
					(byte) 0x48, (byte) 0x00, (byte) 0x84, (byte) 0x00, (byte) 0x84,
					(byte) 0x00, (byte) 0x02, (byte) 0x01, (byte) 0x00, (byte) 0x00,
					(byte) 0x00, (byte) 0x00,});
	private static final Image LEFT_IMG = new Image(10, 16, new byte[]
			{(byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x08,
					(byte) 0x00, (byte) 0x04, (byte) 0x00, (byte) 0xfe, (byte) 0x01,
					(byte) 0x04, (byte) 0x00, (byte) 0x08, (byte) 0x00, (byte) 0x00,
					(byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x08, (byte) 0x00,
					(byte) 0x04, (byte) 0x00, (byte) 0xfe, (byte) 0x01, (byte) 0x04,
					(byte) 0x00, (byte) 0x08, (byte) 0x00, (byte) 0x00, (byte) 0x00,
					(byte) 0x00, (byte) 0x00,});
	private static final Image RIGHT_IMG = new Image(10, 16, new byte[]
			{(byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x40,
					(byte) 0x00, (byte) 0x80, (byte) 0x00, (byte) 0xfe, (byte) 0x01,
					(byte) 0x80, (byte) 0x00, (byte) 0x40, (byte) 0x00, (byte) 0x00,
					(byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x40, (byte) 0x00,
					(byte) 0x80, (byte) 0x00, (byte) 0xfe, (byte) 0x01, (byte) 0x80,
					(byte) 0x00, (byte) 0x40, (byte) 0x00, (byte) 0x00, (byte) 0x00,
					(byte) 0x00, (byte) 0x00,});
	private static final char BACKSPACE = 1;
	private static final char ACCEPT = 2;
	private static final char DECLINE = 3;
	private static final char LEFT = 4;
	private static final char RIGHT = 5;
	private static final char MAX_SPECIAL = RIGHT;
	private static final char[][] MAP = new char[][]{
			{'7', '8', '9', LEFT, RIGHT},
			{'4', '5', '6', BACKSPACE},
			{'1', '2', '3', ACCEPT},
			{'-', '0', '.', DECLINE}
	};
	private int xPos = 0;
	private int yPos = 0;

	@Override
	public boolean graphemeIsImage(int x, int y) {
		return MAP[y][x] <= MAX_SPECIAL;
	}

	@Override
	public Image getGraphemeImage(int x, int y) {
		switch (MAP[y][x]) {
			case BACKSPACE:
				return BACKSPACE_IMG;
			case ACCEPT:
				return ACCEPT_IMG;
			case DECLINE:
				return DECLINE_IMG;
			case LEFT:
				return LEFT_IMG;
			case RIGHT:
				return RIGHT_IMG;
		}
		return null;
	}

	@Override
	public char getGraphemeChar(int x, int y) {
		if (MAP[y][x] > MAX_SPECIAL)
			return MAP[y][x];
		else
			return (char) 0;
	}

	@Override
	public char getSelectedChar() {
		return MAP[yPos][xPos];
	}

	@Override
	public KeyCode getSelectedKey() {
		char ch = getSelectedChar();
		if (ch > MAX_SPECIAL) {
			for (KeyCode code : KeyCode.values()) {
				if (code.toLowerCase() == Character.toLowerCase(ch)) {
					return code;
				}
			}
		} else {
			switch (ch) {
				case BACKSPACE:
					return KeyCode.BACKSPACE;
				case ACCEPT:
					return KeyCode.ACCEPT;
				case DECLINE:
					return KeyCode.EXIT;
				case LEFT:
					return KeyCode.LEFT;
				case RIGHT:
					return KeyCode.RIGHT;
			}
		}
		return KeyCode.UNKNOWN;
	}

	@Override
	public void keyPressed(KeyCode physical) {
		switch (physical) {
			case PHYS_UP:
				kbdUp();
				break;
			case PHYS_DOWN:
				kbdDown();
				break;
			case PHYS_LEFT:
				kbdLeft();
				break;
			case PHYS_RIGHT:
				kbdRight();
				break;
			case PHYS_ENTER:
				kbdEnter();
				break;
		}
	}

	@Override
	public void keyReleased(KeyCode physical) {

	}

	@Override
	public int getX() {
		return xPos;
	}

	@Override
	public int getY() {
		return yPos;
	}

	@Override
	public int getMaxWidth() {
		int max = 0;
		for (char[] aMAP : MAP) {
			if (max < aMAP.length)
				max = aMAP.length;
		}
		return max;
	}

	@Override
	public int getMaxHeight() {
		return MAP.length;
	}

	@Override
	public int getLocalWidth(int y) {
		return MAP[y].length;
	}

	@Override
	public int getLocalHeight(int x) {
		int sum = 0;
		for (char[] aMAP : MAP) {
			if (aMAP.length > x) {
				sum++;
			}
		}
		return sum;
	}

	@Override
	public void reset() {
		yPos = 0;
		xPos = 0;
	}

	private void kbdLeft() {
		if (xPos > 0)
			xPos--;
		else
			xPos = MAP[yPos].length - 1;
	}

	private void kbdRight() {
		if (xPos < MAP[yPos].length - 1)
			xPos++;
		else
			xPos = 0;
	}

	private void kbdUp() {
		if (yPos > 0 && MAP[yPos - 1].length > xPos) {
			yPos--;
		} else {
			for (int y = MAP.length - 1; y >= 0; y--) {
				if (MAP[y].length > xPos) {
					yPos = y;
					break;
				}
			}
		}
	}

	private void kbdDown() {
		if (yPos < MAP.length - 1 && MAP[yPos + 1].length > xPos) {
			yPos++;
		} else {
			for (int y = 0; y < MAP.length; y++) {
				if (MAP[y].length > xPos) {
					yPos = y;
					break;
				}
			}
		}
	}

	private void kbdEnter() {
	}
}
