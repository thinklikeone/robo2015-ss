package thinklikeone.setup;

import lejos.hardware.lcd.GraphicsLCD;
import thinklikeone.gui.Drawable;
import thinklikeone.gui.Platform;

/**
 * Default header
 *
 * @author Jakub Vaněk {@literal <vanek.jakub4@seznam.cz>}
 */
public class Header implements Drawable {
	public Header() {

	}

	private GraphicsLCD getLCD() {
		return Platform.getPlatform().getRenderer().getLCD();
	}

	@Override
	public void draw(int x, int y) {
		GraphicsLCD lcd = getLCD();/*
	    lcd.setColor(GraphicsLCD.WHITE);
		lcd.fillRect(x,y,getWidth(),getHeight());
		lcd.setColor(GraphicsLCD.BLACK);*/
		Battery bat = BatteryImpl.getBattery();
		String battery_voltage = String.format("%.1f", bat.getVoltage()).replace(',', '.');
		lcd.setFont(Main.SMALL_FONT);
		lcd.setColor(GraphicsLCD.BLACK);
		lcd.drawString("* Setup Utility *", 2 + x, 1 + y, GraphicsLCD.TOP | GraphicsLCD.LEFT);
		String batteryString = "BAT:" + battery_voltage + "V";
		boolean inverted = bat.getVoltage() < 7.1f;
		int bx = lcd.getWidth() - Main.SMALL_FONT.stringWidth(batteryString) - 2;
		if (inverted)
			lcd.fillRect(bx - 1 + x, y, 179 - bx, 9);
		lcd.drawString(batteryString, bx + x, 1 + y, GraphicsLCD.TOP | GraphicsLCD.LEFT, inverted);
		lcd.drawLine(x, 9 + y, lcd.getWidth() - x, 9 + y);
	}

	@Override
	public int getWidth() {
		return getLCD().getWidth();
	}

	@Override
	public void setWidth(int w) {
		// noop
	}

	@Override
	public int getHeight() {
		return 11;
	}

	@Override
	public void setHeight(int h) {
		// noop
	}
}
