package thinklikeone.setup;

import lejos.hardware.Brick;
import lejos.hardware.BrickFinder;
import lejos.remote.ev3.RemoteEV3;
import thinklikeone.gui.Platform;
import thinklikeone.gui.SwingGraphicsLCD;
import thinklikeone.gui.SwingPlatform;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;

/**
 * Common program entry-point
 *
 * @author Jakub Vaněk {@literal <vanek.jakub4@seznam.cz>}
 * @author Matej Kripner <kripnermatej@gmail.com>
 */
public class Main implements Runnable {
	public static final lejos.hardware.lcd.Font SMALL_FONT = lejos.hardware.lcd.Font.getSmallFont();
	public static final lejos.hardware.lcd.Font NORMAL_FONT = lejos.hardware.lcd.Font.getDefaultFont();
	public static final String DefaultWinFileName = "C:\\test.cfg"; // TMP
	public static final String DefaultLinuxFileName = "/home/kuba/robo-repo/robo2015-ss/program/src/test.cfg"; // TMP
	public static final String EV3FileName = "/home/lejos/robot.cfg";
	public static boolean EV3;
	private static String OS = System.getProperty("os.name").toLowerCase();
	public boolean InitOK = true;
	private File confFile;
	private EV3SwingPanel swingOutput;
	private JFrame frame;

	public Main(boolean hack) {
		if (hack) {
			Brick brick = null;
			try {
				brick = new RemoteEV3("10.0.1.1");
			} catch (Exception e) {
			}
			BrickFinder.setDefault(brick);
		}
		Platform platform = Platform.getPlatform();
		EV3 = platform != null;
		if (!EV3) { // this is PC
			frame = new JFrame("ThinkLikeOne Setup Utility");
			swingOutput = new EV3SwingPanel(frame);
			frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
			frame.setLayout(new BorderLayout());
			frame.setContentPane(swingOutput);
			frame.pack();
			frame.setLocationRelativeTo(null);
			Platform.setPlatform(new SwingPlatform(frame, swingOutput, swingOutput.img));
			swingOutput.setLCD((SwingGraphicsLCD) Platform.getPlatform().getRenderer().getLCD());
			confFile = openFile();
			if (confFile == null) {
				InitOK = false;
				return;
			}
		} else {
			confFile = new File(EV3FileName);
			platform.getRenderer().getLCD().setAutoRefresh(false);
		}
		if (!confFile.isDirectory()) {
			try {
				confFile.createNewFile();
				Settings.load(confFile);
			} catch (IOException e) {
				System.err.println("IO ERROR: " + e.getMessage());
				try {
					Thread.sleep(500);
				} catch (InterruptedException ex) {
				}
				InitOK = false;
				return;
			}
		} else {
			System.err.println(EV3FileName);
			System.err.println("^^ is dir");
			System.err.println("del it over ssh");
			try {
				Thread.sleep(500);
			} catch (InterruptedException ex) {
			}
			InitOK = false;
			return;
		}
		BatteryImpl.initialize(EV3);
	}

	private static File openFile() {
		File defaultFile = new File(isWindows() ? DefaultWinFileName : DefaultLinuxFileName);
		JFileChooser fc = new JFileChooser(defaultFile.getParentFile());
		fc.setDialogTitle("ThinkLikeOne Setup Utility | Open configuration file");
		fc.setDialogType(JFileChooser.OPEN_DIALOG);
		fc.setFileFilter(new FileNameExtensionFilter("Configuration files", "cfg", "conf", "txt"));
		fc.setAcceptAllFileFilterUsed(false);
		fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
		fc.setSelectedFile(defaultFile);
		int result;
		File f;
		do {
			result = fc.showOpenDialog(null);
			f = fc.getSelectedFile();
		} while (result == JFileChooser.APPROVE_OPTION && !f.exists());
		if (result != JFileChooser.APPROVE_OPTION) {
			return null;
		}
		return f;
	}

	public static void main(String[] args) throws IOException {
		boolean hack = args.length > 0 && args[0].equals("--hack");
		Main main = new Main(hack);
		if (main.InitOK)
			main.run();
	}


	public static boolean isWindows() {

		return (OS.toLowerCase().contains("win"));

	}

	@Override
	public void run() {
		if (!EV3) {
			frame.setVisible(true);
		}
		try {
			GUI.start(confFile);
		} catch (IOException e) {
			throw new IllegalArgumentException(e);
		}
		if (!EV3) {
			frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
		}
	}
}
