package thinklikeone.setup;

import lejos.hardware.Brick;
import lejos.hardware.BrickFinder;

/**
 * Platform-independent battery interface initializer
 *
 * @author Jakub Vaněk {@literal <vanek.jakub4@seznam.cz>}
 */
class BatteryImpl {
	private static Battery impl;

	public static void initialize(boolean EV3) {
		if (EV3)
			impl = new EV3Battery();
		else
			impl = new FakeBattery();
	}

	public static Battery getBattery() {
		return impl;
	}

	/**
	 * Real EV3 battery
	 */
	private static class EV3Battery implements Battery {
		Brick local;

		public EV3Battery() {
			local = BrickFinder.getDefault();
		}

		@Override
		public float getVoltage() {
			return local.getPower().getVoltage();
		}
	}

	/**
	 * Fake battery, constant voltage of 7.8V
	 */
	private static class FakeBattery implements Battery {
		@Override
		public float getVoltage() {
			return 7.8f;
		}
	}
}