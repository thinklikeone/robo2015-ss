package thinklikeone.setup;

import lejos.hardware.lcd.Font;
import lejos.hardware.lcd.GraphicsLCD;
import thinklikeone.gui.*;

/**
 * @author Matej Kripner <kripnermatej@gmail.com>
 * @author Jakub Vaněk {@literal <vanek.jakub4@seznam.cz>}
 * @version 1.0
 */
public class ListContent extends Control {
	private static final Font font = Main.SMALL_FONT;
	private static int MAX_ENTRIES = 10;
	private int selectedIndex = 0;
	private int viewIndex = 0; // FIXME implement view moving
	private boolean shown = false;
	private boolean exit = false;
	private GraphicsLCD lcd;

	public ListContent(GraphicsLCD lcd) {
		this.lcd = lcd;
	}

	public int getSelectedIndex() {
		return selectedIndex;
	}

	public void setSelectedIndex(int selectedIndex) {
		this.selectedIndex = selectedIndex;
	}

	public String getSelectedKey() {
		return (String) Settings.keys()[selectedIndex];
	}

	public void setSelectedKey(String key) {
		Object[] keys = Settings.keys();
		for (int i = 0; i < keys.length; i++) {
			if (key.equals(keys[i])) {
				selectedIndex = i;
				return;
			}
		}
		throw new IllegalArgumentException("Key not found!");
	}

	@Override
	public void draw(int x, int y) {
		//clear();
		final int rowHeight = font.height + 3;
		lcd.setFont(font);
		int Y = y;
		Object[] keys = Settings.keys();
		String key, value;
		Settings.Type type;
		StringBuilder line = new StringBuilder();
		for (int c = 0, i = viewIndex;
		     c < MAX_ENTRIES && i < Settings.size();
		     c++, i++, Y += rowHeight) {
			key = (String) keys[i];
			value = Settings.getString((String) keys[i]);
			type = Settings.getType((String) keys[i]);
			line.delete(0, line.length());
			switch (type) {
				case STRING:
					line.append("STR:");
					break;
				case FLOAT:
					line.append("FLT:");
					break;
				case INT:
					line.append("INT:");
					break;
				case BOOL:
					line.append("BIT:");
					break;
			}
			line.append(key).append("=").append(value);
			// TODO create timer-controlled moving text - use negative x
			if (i == selectedIndex) {
				lcd.setColor(GraphicsLCD.BLACK);
				lcd.fillRect(0, Y, lcd.getWidth(), rowHeight);
			}
			lcd.setColor(GraphicsLCD.BLACK);
			lcd.drawString(line.toString(), x, Y + 2, GraphicsLCD.TOP | GraphicsLCD.LEFT, i == selectedIndex);
			lcd.setColor(GraphicsLCD.BLACK);
			lcd.drawLine(x, Y + rowHeight - 1, lcd.getWidth(), Y + rowHeight - 1);
		}
	}

	private void moveDown() {
		if (selectedIndex + 1 < Settings.size()) {
			selectedIndex++;
			if (selectedIndex - viewIndex + 1 > MAX_ENTRIES)
				viewIndex++;
		} else {
			selectedIndex = 0;
			viewIndex = 0;
		}
	}

	private void moveUp() {
		if (selectedIndex > 0) {
			selectedIndex--;
			if (selectedIndex - viewIndex < 0)
				viewIndex--;
		} else {
			selectedIndex = Settings.size() - 1;
			viewIndex = Math.max(0, selectedIndex - MAX_ENTRIES + 1);
		}
	}

	private void enter() {
		prepareHide();
	}

	private void exit() {
		exit = true;
		prepareHide();
	}

	public boolean exitSelected() {
		return exit;
	}

	public void setExitStatus(boolean exit) {
		this.exit = exit;
	}

	public boolean isClosed() {
		return !shown;
	}

	private void prepareHide() {
		Keyboard kbd = Platform.getPlatform().getKeyboard();
		kbd.deactivate();
		shown = false;
	}

	public void prepareShow() {
		Keyboard kbd = Platform.getPlatform().getKeyboard();
		kbd.setPhysical(true);
		kbd.activate();
		shown = true;
		exit = false;
	}

	@Override
	public void eventHandler(Event event) {
		KeyboardEvent<KeyCode> kbdEvent = KeyboardEvent.castToKeyboardEvent(event);
		if (kbdEvent == null || kbdEvent.getType() == KeyboardEvent.Type.RELEASE)
			return;
		switch (kbdEvent.getKey()) {
			case PHYS_UP:
				moveUp();
				break;
			case PHYS_DOWN:
				moveDown();
				break;
			case PHYS_ENTER:
				enter();
				break;
			case PHYS_ESCAPE:
				exit();
				break;
		}
	}
}
