package thinklikeone.setup;

import lejos.hardware.lcd.GraphicsLCD;
import thinklikeone.gui.Control;
import thinklikeone.gui.Platform;
import thinklikeone.gui.Renderer;

/**
 * Input dialog
 *
 * @author Jakub Vaněk {@literal <vanek.jakub4@seznam.cz>}
 */
public abstract class Entry<T> extends Control {
	/**
	 * Top-left alignmentation mask
	 */
	public static final int TOP_LEFT = GraphicsLCD.TOP | GraphicsLCD.LEFT;
	/**
	 * Top-right alignmentation mask
	 */
	public static final int TOP_RIGHT = GraphicsLCD.TOP | GraphicsLCD.RIGHT;
	/**
	 * Bottom-left alignmentation mask
	 */
	public static final int BOTTOM_LEFT = GraphicsLCD.BOTTOM | GraphicsLCD.LEFT;
	/**
	 * Bottom-right alignmentation mask
	 */
	public static final int BOTTOM_RIGHT = GraphicsLCD.BOTTOM | GraphicsLCD.RIGHT;
	/**
	 * Whether this control is closed
	 */
	protected boolean closed = true;
	/**
	 * Output framebuffer
	 */
	protected GraphicsLCD lcd;
	/**
	 * Name of value
	 */
	protected String name = "N/A";

	public Entry() {
		lcd = Platform.getPlatform().getRenderer().getLCD();
	}

	public Entry(Renderer render) {
		lcd = render.getLCD();
	}

	/**
	 * Prepare this entry for displaying.
	 *
	 * @param value Starting value.
	 */
	public abstract void open(T value, String name);

	/**
	 * Check whether this control was isClosed by a keypress.
	 *
	 * @return True if isClosed.
	 */
	public final boolean isClosed() {
		return closed;
	}

	/**
	 * Get value entered into this entry.
	 *
	 * @return Actual value.
	 */
	public abstract T getValue();

	/**
	 * Set value inside this entry.
	 *
	 * @param value Value to set.
	 */
	public abstract void setValue(T value);

	/**
	 * Get value name.
	 *
	 * @return Name.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Set value name.
	 *
	 * @param value Name.
	 */
	public void setName(String value) {
		this.name = value;
	}

	/**
	 * Draws a name using small font with top-left corner in [2,1]
	 */
	protected void drawName(int x, int y) {
		lcd.setFont(Main.SMALL_FONT);
		String prompt = name + ":";
		lcd.drawString(prompt, 2 + x, 1 + y, TOP_LEFT);
	}

	public abstract boolean valueChanged();
}
