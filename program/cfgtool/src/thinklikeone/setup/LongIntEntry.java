package thinklikeone.setup;

import lejos.hardware.lcd.GraphicsLCD;
import thinklikeone.gui.Event;
import thinklikeone.gui.KeyCode;
import thinklikeone.gui.KeyboardEvent;
import thinklikeone.gui.Platform;

/**
 * Dialog for entering integer numbers by editing single figures.
 *
 * @author Jakub Vaněk {@literal <vanek.jakub4@seznam.cz>}
 */
public class LongIntEntry extends Entry<Integer> {
	private static LongIntEntry instance;
	private int original = 0;
	private int value = 0;
	private int position = 0;

	public LongIntEntry(GraphicsLCD surface) {
		this.lcd = surface;
	}

	@Override
	public void draw(int x, int y) {
		drawName(x, y);
		lcd.drawRect(1 + x, Main.SMALL_FONT.height + 3 + y, 175, Main.NORMAL_FONT.height + 4);
		lcd.setFont(Main.NORMAL_FONT);
		for (int i = 0; i < 10; i++) {
			int digit = Math.abs((value / (int) Math.pow(10, i)) % 10);
			int digitx = 174 - Main.NORMAL_FONT.width * i;
			String str = new String(new char[]{(char) (digit + 48)});
			lcd.drawString(str, digitx + x, Main.SMALL_FONT.height + 6 + y, TOP_RIGHT, i == position);
		}
		String sign = value < 0 ? "-" : "+";
		lcd.drawString(sign, 174 - Main.NORMAL_FONT.width * 10 + x, Main.SMALL_FONT.height + 6 + y,
				TOP_RIGHT, position == 10);
	}

	@Override
	public void open(Integer value, String name) {
		this.value = value;
		this.original = value;
		this.position = 0;
		this.name = name;
		this.closed = false;
		Platform.getPlatform().getKeyboard().setPhysical(true);
		Platform.getPlatform().getKeyboard().activate();
	}

	@Override
	public Integer getValue() {
		return value;
	}

	@Override
	public void setValue(Integer value) {
		this.value = value;
		this.original = value;
	}

	@Override
	public boolean valueChanged() {
		return original != value;
	}

	@Override
	public void eventHandler(Event event) {
		try {
			if (event instanceof KeyboardEvent) {
				KeyboardEvent<KeyCode> kbdEvent = (KeyboardEvent<KeyCode>) event;
				if (kbdEvent.getType() == KeyboardEvent.Type.PRESS) {
					switch (kbdEvent.getKey()) {
						case PHYS_UP:
							if (position == 10) {
								if (value == Integer.MIN_VALUE) // to prevent same number (two's complement overflow)
									value++;
								if (value == 0)
									value = -1;
								else
									value *= -1;
							} else {
								if (value >= 0) {
									long power = ((int) Math.pow(10, position));
									long newVal = power + value;
									if (newVal <= Integer.MAX_VALUE) {
										value = (int) newVal;
									}
								} else {
									long power = (int) Math.pow(10, position);
									long newVal = value - power;
									if (newVal >= Integer.MIN_VALUE) {
										value = (int) newVal;
									}
								}
							}
							break;
						case PHYS_DOWN:
							if (position == 10) {
								if (value == Integer.MIN_VALUE) // to prevent same number (two's complement overflow)
									value++;
								if (value == 0)
									value = -1;
								else
									value *= -1;
							} else {
								if (value > 0) {
									long power = (int) Math.pow(10, position);
									long newVal = value - power;
									int newSign = Long.signum(newVal);
									if (newSign != -1) {
										if (newVal >= Integer.MIN_VALUE) {
											value = (int) newVal;
										}
									}

								} else if (value < 0) {
									long power = ((int) Math.pow(10, position));
									long newVal = power + value;
									int newSign = Long.signum(newVal);
									if (newSign != 1) {
										if (newVal <= Integer.MAX_VALUE) {
											value = (int) newVal;
										}
									}
								}
							}
							break;
						case PHYS_RIGHT:
							if (position > 0)
								position--;
							else
								position = 10;
							break;
						case PHYS_LEFT:
							if (position < 10)
								position++;
							else
								position = 0;
							break;
						case PHYS_ENTER:
							closed = true;
							Platform.getPlatform().getKeyboard().deactivate();
							break;
						case PHYS_ESCAPE:
							value = original;
							closed = true;
							Platform.getPlatform().getKeyboard().deactivate();
							break;
					}
				}
			}
		} catch (ClassCastException e) {
			System.err.println("BUG! " + e.getMessage());
		}
	}
}