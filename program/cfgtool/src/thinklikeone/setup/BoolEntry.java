package thinklikeone.setup;

import lejos.hardware.lcd.GraphicsLCD;
import thinklikeone.gui.Event;
import thinklikeone.gui.KeyCode;
import thinklikeone.gui.KeyboardEvent;
import thinklikeone.gui.Platform;


/**
 * Dialog for entering on/off (boolean) values
 *
 * @author Jakub Vaněk {@literal <vanek.jakub4@seznam.cz>}
 */
public class BoolEntry extends Entry<Boolean> {
	private static BoolEntry instance = null;
	private boolean value = false;
	private boolean original = false;

	public BoolEntry(GraphicsLCD surface) {
		this.lcd = surface;
	}

	@Override
	public void draw(int x, int y) {
		//clear();
		drawName(x, y);
		lcd.drawRect(10 + x, 15 + y, 16, 16);
		if (value) {
			lcd.fillRect(12 + x, 17 + y, 13, 13);
		}
		lcd.setFont(Main.NORMAL_FONT);
		lcd.drawString(Boolean.toString(value), 32 + x, 16 + y, TOP_LEFT);
	}

	@Override
	public void open(Boolean value, String name) {
		this.value = value;
		this.original = value;
		this.name = name;
		this.closed = false;
		Platform.getPlatform().getKeyboard().setPhysical(true);
		Platform.getPlatform().getKeyboard().activate();
	}

	@Override
	public Boolean getValue() {
		return value;
	}

	@Override
	public void setValue(Boolean value) {
		this.value = value;
		this.original = value;
	}

	@Override
	public boolean valueChanged() {
		return original != value;
	}

	@Override
	public void eventHandler(Event event) {
		try {
			if (event instanceof KeyboardEvent) {
				KeyboardEvent<KeyCode> kbdEvent = (KeyboardEvent<KeyCode>) event;
				if (kbdEvent.getType() == KeyboardEvent.Type.PRESS) {
					switch (kbdEvent.getKey()) {
						case PHYS_UP:
						case PHYS_DOWN:
							value = !value;
							break;
						case PHYS_ENTER:
							closed = true;
							Platform.getPlatform().getKeyboard().deactivate();
							break;
						case PHYS_ESCAPE:
							value = original;
							closed = true;
							Platform.getPlatform().getKeyboard().deactivate();
							break;
					}
				}
			}
		} catch (ClassCastException e) {
			System.err.println("BUG! " + e.getMessage());
		}
	}
}
