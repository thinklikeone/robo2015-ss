package main.test;

import lejos.hardware.Button;
import main.Settings;
import main.robot.RealRobotics;

import java.io.File;

import static main.Main.CONFIG_FILE;

/**
 * SuperPilot test class
 */
public class SuperPilotTest {
	public static void main(String[] args) throws Exception {
		Button.LEDPattern(3);
		Settings.update(new File(CONFIG_FILE));
		RealRobotics robotics = new RealRobotics();
		Button.LEDPattern(2);
		robotics.calibrateGyro();
		Button.LEDPattern(1);
		Button.ENTER.waitForPress();
		Button.LEDPattern(0);
		robotics.rotate(90, true);
		robotics.flush();
		robotics.rotate(-180, true);
		robotics.flush();
		robotics.rotate(-270, true);
		robotics.flush();
		Button.LEDPattern(0);
	}
}
