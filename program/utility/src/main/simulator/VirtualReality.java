package main.simulator;

import lejos.robotics.geometry.Point;
import main.Robotics;
import main.Settings;
import main.robot.ai.map.Map;

import java.util.HashSet;
import java.util.Set;

/**
 * Virtual reality emulation
 */
public class VirtualReality {
	private static final int SONIC_MAX = 2000;
	private final Object pause_lock;
	private final Object heartbeat;
	private int robotX;
	private int robotY;
	private int robotAzimuth;
	private int robotWidth;
	private int robotLength;
	private float move_delayPerMm;
	private float rotate_delayPerDeg;
	private RealField[][] realMap;
	private Magnet magnet;
	private int magnet_wait;
	private int mm_per_deg;
	private boolean pause;

	public VirtualReality() {
		realMap = new RealField[Map.WIDTH][Map.HEIGHT];
		for (int x = 0; x < Map.WIDTH; x++) {
			for (int y = 0; y < Map.HEIGHT; y++) {
				realMap[x][y] = RealField.LIGHT_OFF;
			}
		}
		realMap[3][3] = realMap[4][3] = realMap[5][3] = RealField.WALL;
		robotWidth = 200;
		robotLength = 200;
		move_delayPerMm = 3;
		rotate_delayPerDeg = 10;
		mm_per_deg = 1;
		pause_lock = new Object();
		heartbeat = new Object();
		reset();
	}

	// source: http://stackoverflow.com/questions/16314069/calculation-of-intersections-between-line-segments
	public static Point lineIntersect(Point l1p1, Point l1p2, Point l2p1, Point l2p2) {
		double denom = (l2p2.y - l2p1.y) * (l1p2.x - l1p1.x) - (l2p2.x - l2p1.x) * (l1p2.y - l1p1.y);
		if (denom == 0.0) { // Lines are parallel.
			return null;
		}
		double ua = ((l2p2.x - l2p1.x) * (l1p1.y - l2p1.y) - (l2p2.y - l2p1.y) * (l1p1.x - l2p1.x)) / denom;
		double ub = ((l1p2.x - l1p1.x) * (l1p1.y - l2p1.y) - (l1p2.y - l1p1.y) * (l1p1.x - l2p1.x)) / denom;
		if (ua >= 0.0f && ua <= 1.0f && ub >= 0.0f && ub <= 1.0f) {
			// Get the intersection point.
			return new Point((int) (l1p1.x + ua * (l1p2.x - l1p1.x)), (int) (l1p1.y + ua * (l1p2.y - l1p1.y)));
		}

		return null;
	}

	public void reset() {
		robotX = Settings.INT.FIELD_SIZE.getValue() * 4 + (Settings.INT.FIELD_SIZE.getValue() - robotWidth) / 2;
		robotY = Settings.INT.FIELD_SIZE.getValue() * 3 + (Settings.INT.FIELD_SIZE.getValue() - robotLength) / 2;
		robotAzimuth = 0;
		pause = false;
		magnet = Magnet.Up;
	}

	public synchronized boolean isPaused() {
		return pause;
	}

	public synchronized void setPause(boolean pause) {
		this.pause = pause;
	}

	public synchronized int getRobotX() {
		return robotX;
	}

	public synchronized void setRobotX(int robotX) {
		this.robotX = robotX;
		synchronized (heartbeat) {
			heartbeat.notify();
		}
	}

	public synchronized void addToRobotX(double val) {
		robotX += val;
		synchronized (heartbeat) {
			heartbeat.notify();
		}
	}

	public synchronized int getRobotY() {
		return robotY;
	}

	public synchronized void setRobotY(int robotY) {
		this.robotY = robotY;
		synchronized (heartbeat) {
			heartbeat.notify();
		}
	}

	public synchronized void addToRobotY(double val) {
		robotY += val;
		synchronized (heartbeat) {
			heartbeat.notify();
		}
	}

	public synchronized int getRobotAzimuth() {
		return robotAzimuth;
	}

	public synchronized void setRobotAzimuth(int robotAzimuth) {
		this.robotAzimuth = robotAzimuth;
		synchronized (heartbeat) {
			heartbeat.notify();
		}
	}

	public synchronized void addToRobotAzimuth(double val) {
		robotAzimuth += val;
		synchronized (heartbeat) {
			heartbeat.notify();
		}
	}

	public synchronized int getRobotWidth() {
		return robotWidth;
	}

	public synchronized void setRobotWidth(int robotWidth) {
		this.robotWidth = robotWidth;
		synchronized (heartbeat) {
			heartbeat.notify();
		}
	}

	public synchronized int getRobotLength() {
		return robotLength;
	}

	public synchronized void setRobotLength(int robotLength) {
		this.robotLength = robotLength;
		synchronized (heartbeat) {
			heartbeat.notify();
		}
	}

	public synchronized float getMove_delayPerMm() {
		return move_delayPerMm;
	}

	public synchronized void setMove_delayPerMm(float move_delayPerMm) {
		this.move_delayPerMm = move_delayPerMm;
	}

	public synchronized float getRotate_delayPerDeg() {
		return rotate_delayPerDeg;
	}

	public synchronized void setRotate_delayPerDeg(float rotate_delayPerDeg) {
		this.rotate_delayPerDeg = rotate_delayPerDeg;
	}

	public synchronized RealField[][] getRealMap() {
		return realMap;
	}

	public synchronized void setRealMap(RealField[][] realMap) {
		this.realMap = realMap;
		synchronized (heartbeat) {
			heartbeat.notify();
		}
	}

	public synchronized Magnet getMagnet() {
		return magnet;
	}

	public synchronized void setMagnet(Magnet magnet) {
		this.magnet = magnet;
		synchronized (heartbeat) {
			heartbeat.notify();
		}
	}

	public synchronized boolean touchPressed() {
		return true; // to prevent infinite wait
	}

	public synchronized float getSonic() {
		// 1. calc US position and the furthest measurable point
		// 2. clip the map
		// 3. check intersections
		// 3. return the lowest measurement
		double vec_x = -Math.sin(Math.toRadians(getRobotAzimuth()));
		double vec_y = -Math.cos(Math.toRadians(getRobotAzimuth()));
		int x1 = getRobotX() + (int) Math.round(vec_x * getRobotLength() / 2);
		int y1 = getRobotY() + (int) Math.round(vec_y * getRobotLength() / 2);
		Point us = new Point(x1, y1);
		int x2 = getRobotX() + (int) Math.round(vec_x * SONIC_MAX);
		int y2 = getRobotY() + (int) Math.round(vec_y * SONIC_MAX);
		Point far = new Point(x2, y2);
		Set<Point[]> list = new HashSet<>();
		final int tile_size = Settings.INT.FIELD_SIZE.getValue();
		for (int x = 0; x < Map.WIDTH; x++) {
			for (int y = 0; y < Map.HEIGHT; y++) {
				if (realMap[x][y] == RealField.WALL) {
					list.add(new Point[]{
							new Point(x * tile_size, y * tile_size), new Point((x + 1) * tile_size, y * tile_size)
					});// top
					list.add(new Point[]{
							new Point(x * tile_size, (y + 1) * tile_size), new Point((x + 1) * tile_size, (y + 1) * tile_size)
					});// bottom
					list.add(new Point[]{
							new Point(x * tile_size, y * tile_size), new Point(x * tile_size, (y + 1) * tile_size)
					});// left
					list.add(new Point[]{
							new Point((x + 1) * tile_size, y * tile_size), new Point((x + 1) * tile_size, (y + 1) * tile_size)
					});// right
				}
			}
		}
		list.add(new Point[]{new Point(0, 0),
				new Point(Map.WIDTH * tile_size, 0)});
		list.add(new Point[]{new Point(0, Map.HEIGHT * tile_size),
				new Point(Map.WIDTH * tile_size, Map.HEIGHT * tile_size)});
		list.add(new Point[]{new Point(0, 0),
				new Point(0, Map.HEIGHT * tile_size)});
		list.add(new Point[]{new Point(Map.WIDTH * tile_size, 0),
				new Point(Map.WIDTH * tile_size, Map.HEIGHT * tile_size)});
		float min_dist = 2550;
		for (Point[] pair : list) {
			Point intersect = lineIntersect(us, far, pair[0], pair[1]);
			if (intersect == null)
				continue;
			float dist = (float) Math.sqrt(Math.pow(us.x - intersect.x, 2) + Math.pow(us.y - intersect.y, 2));
			if (dist < min_dist)
				min_dist = dist;
		}
		if (min_dist == 2550)
			return Float.POSITIVE_INFINITY;
		return min_dist;
	}

	public synchronized Robotics.Light getColor() {
		// warn simulates real problems - robot may not be on light
		int nearest_x = (getRobotX() / Settings.INT.FIELD_SIZE.getValue()) // important: integer division
				* Settings.INT.FIELD_SIZE.getValue() + Settings.INT.FIELD_SIZE.getValue() / 2;
		int nearest_y = (getRobotY() / Settings.INT.FIELD_SIZE.getValue())
				* Settings.INT.FIELD_SIZE.getValue() + Settings.INT.FIELD_SIZE.getValue() / 2;
		// todo simulate robot with sensor not in center
		int radius_square = Settings.INT.LIGHT_SIZE.getValue() * Settings.INT.LIGHT_SIZE.getValue();
		int dist_square = Math.abs(nearest_x - getRobotX()) + Math.abs(nearest_y - getRobotY());
		boolean collision = radius_square > dist_square;
		if (collision) {
			RealField real = realMap[getRobotX() / Settings.INT.FIELD_SIZE.getValue()][
					getRobotY() / Settings.INT.FIELD_SIZE.getValue()];
			if (real == RealField.LIGHT_RED)
				return Robotics.Light.RED;
			else
				return Robotics.Light.OTHER;
		} else {
			return Robotics.Light.OTHER;
		}
	}

	public synchronized int getMagnet_wait() {
		return magnet_wait;
	}

	public synchronized void setMagnet_wait(int magnet_wait) {
		this.magnet_wait = magnet_wait;
	}

	public synchronized int getMm_per_deg() {
		return mm_per_deg;
	}

	public synchronized void setMm_per_deg(int mm_per_deg) {
		this.mm_per_deg = mm_per_deg;
	}

	public synchronized Object getPause_lock() {
		return pause_lock;
	}

	public synchronized int getGyro() {
		return robotAzimuth;
	}

	public Object getHeartbeat() {
		return heartbeat;
	}

	public enum Magnet {
		Up, Down
	}
}
