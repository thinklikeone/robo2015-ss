package main.simulator;


import main.Robotics;
import main.Settings;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

/**
 * Main class for running program with fake EV3 interface
 *
 * @author Matej Kripner <kripnermatej@gmail.com>
 * @version 1.0
 */
public class SimulatorMain {
	public static void main(String[] args) {
		JFileChooser fc = new JFileChooser();
		fc.setDialogTitle("Load settings");
		int result;
		File f;
		do {
			result = fc.showOpenDialog(null);
			f = fc.getSelectedFile();
		} while (result == JFileChooser.APPROVE_OPTION && !f.exists());
		if (result != JFileChooser.APPROVE_OPTION) {
			return;
		}
		try {
			Settings.update(f);
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
		final VirtualReality reality = new VirtualReality();
		Robotics.setRobotics(new SimulatorRobotics(reality));
		try {
			SwingUtilities.invokeAndWait(new Runnable() {
				@Override
				public void run() {
					MainForm form = new MainForm(reality);
				}
			});
		} catch (InterruptedException | InvocationTargetException e) {
			e.printStackTrace();
		}
		// TODO implementation:
		// 1. create a Runnable with main program
		// 2. initialize and show GUI
		// 3. start robot program thread
		// 4. do interaction robot program thread <-> GUI thread through VirtualReality
	}
}
