package main.simulator;

import main.Robotics;
import main.Settings;

/**
 * Simulator linking point.
 *
 * @author Jakub Vaněk {@literal vanek.jakub4@seznam.cz}
 * @version 1.0
 */
public class SimulatorRobotics extends Robotics {
	float gyroOffset = 0;
	float calibOffset = 0;
	private VirtualReality reality;
	private int lastTravel = 0;

	public SimulatorRobotics(VirtualReality reality) {
		this.reality = reality;
	}

	@Override
	public void moveForward(int millimeters, boolean block) {
		lastTravel = millimeters;
		double vec_x = -Math.sin(Math.toRadians(reality.getRobotAzimuth())) * millimeters;
		double vec_y = -Math.cos(Math.toRadians(reality.getRobotAzimuth())) * millimeters;

		if (block && !reality.isPaused())
			delay(Math.round(reality.getMove_delayPerMm() * Math.abs(millimeters)));
		else checkPause();
		reality.addToRobotX(vec_x);
		reality.addToRobotY(vec_y);
	}

	private void delay(long delay) {
		try {
			Thread.sleep(delay);
		} catch (InterruptedException ie) {
			throw new IllegalStateException("program stop");
		}
	}

	private void checkPause() {
		if (reality.isPaused()) {
			try {
				synchronized (reality.getPause_lock()) {
					reality.getPause_lock().wait();
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void moveBackward(int millimeters, boolean block) {
		moveForward(-millimeters, block);
	}

	@Override
	public void startMoveForward() {
		// todo
		moveToWall(false);
		checkPause();
	}

	@Override
	public void startMoveBackward() {
		// todo
		checkPause();
	}

	@Override
	public void stop() {
		// todo
		checkPause();
	}

	@Override
	public void rotate(int degrees, boolean block) {
		if (block && !reality.isPaused())
			delay(Math.round(reality.getRotate_delayPerDeg() * Math.abs(degrees)));
		else checkPause();
		reality.addToRobotAzimuth(degrees);
		lastTravel = degrees * reality.getMm_per_deg();
	}

	@Override
	public float getMoveVelocity() {
		return 280;
	}

	@Override
	public void setMoveVelocity(float mm_per_sec) {

	}

	@Override
	public void setMoveAcceleration(float mm_per_sec_squared) {

	}

	@Override
	public float getRotationVelocity() {
		return 140;
	}

	@Override
	public void setRotationVelocity(float deg_per_sec) {

	}

	@Override
	public void setRotationAcceleration(float deg_per_sec_squared) {

	}

	@Override
	public boolean frontTouch() {
		return reality.touchPressed();
	}

	@Override
	public float leftSonic() {
		return reality.getSonic();
	}

	@Override
	public float gyro() {
		return reality.getGyro() - gyroOffset - calibOffset;
	}

	@Override
	public void calibrateGyro() {
		calibOffset = reality.getRobotAzimuth();
	}

	@Override
	public float getGyroOffset() {
		return gyroOffset;
	}

	@Override
	public void setGyroOffset(float value) {
		gyroOffset = value;
	}

	@Override
	public void magnetDown(boolean block) {
		if (block && !reality.isPaused())
			delay(reality.getMagnet_wait());
		else checkPause();
		reality.setMagnet(VirtualReality.Magnet.Down);
	}

	@Override
	public void magnetUp(boolean block) {
		if (block && !reality.isPaused())
			delay(reality.getMagnet_wait());
		else checkPause();
		reality.setMagnet(VirtualReality.Magnet.Up);
	}

	@Override
	public boolean stoppedByTouch() {
		return false; // todo
	}

	@Override
	public Light detectLight() {
		return reality.getColor();
	}

	@Override
	public void moveToWall(boolean block) {
		// todo simulate crash
		int approx_x = reality.getRobotX() / Settings.INT.FIELD_SIZE.getValue();
		int approx_y = reality.getRobotY() / Settings.INT.FIELD_SIZE.getValue();
		final int shift = (Settings.INT.FIELD_SIZE.getValue() - reality.getRobotLength()) / 2;
		Pos approx = new Pos(approx_x, approx_y);
		int newRobotX = reality.getRobotX();
		int newRobotY = reality.getRobotY();
		int azimuth = reality.getRobotAzimuth();
		if (azimuth < 45 && azimuth > -45) { // go north
			int free = beforeWall(MapOrient.North, approx);
			approx_y -= free;
			newRobotY = approx_y * Settings.INT.FIELD_SIZE.getValue() - shift;
		} else if (azimuth > 45 && azimuth < 135) { // go west
			int free = beforeWall(MapOrient.West, approx);
			approx_x -= free;
			newRobotX = approx_x * Settings.INT.FIELD_SIZE.getValue() - shift;
		} else if (azimuth < -45 && azimuth > -135) { // go east
			int free = beforeWall(MapOrient.East, approx);
			approx_x += free;
			newRobotX = approx_x * Settings.INT.FIELD_SIZE.getValue() + shift;
		} else if (azimuth > 135 || azimuth < -135) { // go south
			int free = beforeWall(MapOrient.South, approx);
			approx_y += free;
			newRobotY = approx_y * Settings.INT.FIELD_SIZE.getValue() + shift;
		}
		int travel = Math.max(Math.abs(newRobotX - reality.getRobotX()), Math.abs(newRobotY - reality.getRobotY()));
		if (block && !reality.isPaused())
			delay((long) (travel * reality.getMove_delayPerMm()));
		else checkPause();
		reality.setRobotX(newRobotX);
		reality.setRobotY(newRobotY);
	}

	/**
	 * Imported from TuxTARDIS
	 */
	private int beforeWall(int orient, Pos pos) {
		int counter = 0;
		Pos now = Pos.modify(pos, orient, 1);
		RealField tile = reality.getRealMap()[now.x][now.y];
		while (tile != RealField.WALL) {
			now = Pos.modify(now, orient, 1);
			try {
				tile = reality.getRealMap()[now.x][now.y];
			} catch (IndexOutOfBoundsException e) {
				tile = RealField.WALL;
			}
			counter++;
		}
		return counter;
	}

	@Override
	public boolean isMoving() {
		return false;
	}

	@Override
	public float getDistanceTravelledLeft() {
		return lastTravel;
	}

	@Override
	public void free() {
		// todo
	}

	@Override
	public float getDistanceTravelledRight() {
		return lastTravel;
	}

	@Override
	public void flush() {

	}

	@Override
	public void waitForStop() {

	}

	public void reset() {
		gyroOffset = 0;
		calibOffset = 0;
		lastTravel = 0;
	}
}
