package main.simulator;

import main.Robotics;
import main.Settings;
import main.robot.ai.AI;
import main.robot.ai.map.Field;
import main.robot.ai.map.Map;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;
import java.nio.file.StandardOpenOption;

/**
 * Main form
 */
public class MainForm extends JFrame {
	private static final int tileSize = 50;
	private static final int border = 1;
	private static final int separator = 20;
	private static final int mainBorder = 10;
	private static final int MAX_DELAY = 50;
	private static final int MIN_DELAY = 1;
	private JRadioButton testModeRadioButton;
	private JRadioButton editModeRadioButton;
	private JButton runResetButton;
	private JButton stopButton;
	private JPanel panel;
	private JPanel rootPanel;
	private JButton pauseButton;
	private JLabel infoLabel;
	private JPanel testControls;
	private JButton loadButton;
	private JButton saveButton;
	private JPanel editControls;
	private JSpinner spinner1;
	private VirtualReality reality;
	private Mode mode;
	private boolean stopped = true;
	private Thread programThread;

	public MainForm(VirtualReality reality) {
		super("ThinkLikeOne program simulator");
		this.reality = reality;
		RadioListener radioListener = new RadioListener();
		testModeRadioButton.setActionCommand("test");
		editModeRadioButton.setActionCommand("edit");
		testModeRadioButton.addActionListener(radioListener);
		editModeRadioButton.addActionListener(radioListener);
		runResetButton.addActionListener(new RunListener());
		pauseButton.addActionListener(new PauseListener());
		stopButton.addActionListener(new StopListener());
		loadButton.addActionListener(new MapLoader());
		saveButton.addActionListener(new MapSaver());
		DelayUpdater delay_handler = new DelayUpdater();
		spinner1.addChangeListener(delay_handler);
		spinner1.setValue(3);
		delay_handler.stateChanged(null);
		Thread endpoint = new Thread(new HeartbeatListener(reality, panel));
		endpoint.start();
		setMode(Mode.EDIT);
		setContentPane(rootPanel);
		pack();
		setResizable(false);
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setVisible(true);
	}

	private void createUIComponents() {
		panel = new MainPanel();
	}

	private void setMode(Mode mode) {
		this.mode = mode;
		MainPanel mainPanel = (MainPanel) panel;
		switch (mode) {
			case TEST:
				panel.setPreferredSize(new Dimension(mainPanel.mapWidth + 2 * mainBorder,
						mainPanel.mapHeight * 2 + separator + 2 * mainBorder));
				testControls.setVisible(true);
				editControls.setVisible(false);
				break;
			case EDIT:
				stopProgram();
				panel.setPreferredSize(new Dimension(mainPanel.mapWidth + 2 * mainBorder,
						mainPanel.mapHeight + 2 * mainBorder));
				testControls.setVisible(false);
				editControls.setVisible(true);
				break;
		}
		setInfoPosition(-1, -1);
		this.pack();
	}

	private void setInfoPosition(int x, int y) {
		if (mode == Mode.EDIT) {
			if (x >= 0 && x < Map.WIDTH && y >= 0 && y < Map.HEIGHT) {
				String type = "<error>";
				switch (reality.getRealMap()[x][y]) {
					case LIGHT_GREEN:
						type = "green light";
						break;
					case LIGHT_RED:
						type = "red light";
						break;
					case LIGHT_OFF:
						type = "off light";
						break;
					case WALL:
						type = "wall";
						break;
				}
				infoLabel.setText("Field[" + x + "," + y + "] = " + type);
			} else {
				infoLabel.setText("Click inside map to edit the field.");
			}
		} else if (mode == Mode.TEST) {
			infoLabel.setText("Test mode");
		}
	}

	private void setTile(int x, int y, RealField f) {
		reality.getRealMap()[x][y] = f;
		if (mode == Mode.EDIT)
			setInfoPosition(x, y);
		panel.repaint();
	}

	private void stopProgram() {
		stopped = true;
		if (programThread != null) {
			synchronized (reality.getPause_lock()) {
				reality.getPause_lock().notifyAll();
			}
			while (!Thread.currentThread().isInterrupted() &&
					programThread != null &&
					programThread.isAlive())
				programThread.interrupt();
		}
	}

	private void startProgram() {
		stopProgram();
		((SimulatorRobotics) Robotics.getRobotics()).reset();
		reality.reset();
		Map.getMap().reset();
		stopped = false;
		programThread = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					AI program = new AI();
					program.run();
				} catch (IllegalStateException e) {

				}
				programThread = null;
				stopProgram();
			}
		});
		programThread.setName("AI thread");
		programThread.start();
	}

	enum Mode {
		TEST, EDIT
	}

	private class MainPanel extends JPanel {
		private final int cellSize = tileSize + border;
		int mapWidth = (tileSize + border) * Map.WIDTH + border;
		int mapHeight = (tileSize + border) * Map.HEIGHT + border;
		int mouseX = 0;
		int mouseY = 0;
		int edit_field_x = -1;
		int edit_field_y = -1;

		public MainPanel() {
			super();
			final MouseMotionAdapter motionAdapter = new MouseMotionAdapter() {
				@Override
				public void mouseMoved(MouseEvent e) {
					mouseX = e.getX();
					mouseY = e.getY();
					if (mode == Mode.EDIT) {
						int tile_x = getTileX(e, mainBorder);
						int tile_y = getTileY(e, mainBorder);
						if (tile_x != edit_field_x || tile_y != edit_field_y) {
							edit_field_x = tile_x;
							edit_field_y = tile_y;
							setInfoPosition(tile_x, tile_y);
							repaint();
						}
					} else if (mode == Mode.TEST) {
						// todo
						//repaint();
					}
				}
			};
			addMouseListener(new MouseAdapter() { // todo input handling
				@Override
				public void mouseClicked(MouseEvent e) {
					if (mode == Mode.EDIT) {
						int tile_x = getTileX(e, mainBorder);
						int tile_y = getTileY(e, mainBorder);
						if (tile_x < 0 || tile_x >= Map.WIDTH ||
								tile_y < 0 || tile_y >= Map.HEIGHT)
							return;
						EditPopupMenu menu = new EditPopupMenu(tile_x, tile_y);
						menu.show(MainPanel.this, e.getX(), e.getY());
					} else if (mode == Mode.TEST) {
						if (e.getX() > mainBorder && e.getX() < mainBorder + mapWidth) {
							int x = e.getX() - mainBorder;
							int y;
							if (e.getY() > mainBorder && e.getY() < mainBorder + mapHeight) {
								y = e.getY() - mainBorder;
							} else if (e.getY() > mainBorder + mapHeight + separator && e.getY() < mainBorder + mapHeight * 2 + separator) {
								y = e.getY() - mainBorder - mapHeight - separator;
							} else {
								return;
							}
							ProbabilityInfoPopupMenu info = new ProbabilityInfoPopupMenu(x / cellSize, y / cellSize);
							info.show(MainPanel.this, e.getX(), e.getY());
						}
					}
				}

				@Override
				public void mouseExited(MouseEvent e) {
					edit_field_x = -1;
					edit_field_y = -1;
					if (mode == Mode.EDIT)
						setInfoPosition(-1, -1);
				}

				@Override
				public void mouseEntered(MouseEvent e) {
					edit_field_x = -1;
					edit_field_y = -1;
					if (mode == Mode.EDIT)
						setInfoPosition(-1, -1);
				}
			});
			addMouseMotionListener(motionAdapter);
		}

		private int getTileX(MouseEvent e, int subtract) {
			return (e.getX() - subtract) / cellSize;
		}

		private int getTileY(MouseEvent e, int subtract) {
			return (e.getY() - subtract) / cellSize;
		}

		@Override
		public void paintComponent(Graphics front) {
			super.paintComponent(front);
			switch (mode) {
				case TEST: // draw probability and physical map
					drawPhysicalMap(mainBorder, mainBorder, front);
					drawRobot(mainBorder, mainBorder, front, 255);
					drawProbabilityMap(mainBorder, mainBorder + mapHeight + separator, front);
					drawRobot(mainBorder, mainBorder + mapHeight + separator, front, 150);
					break;
				case EDIT: // draw physical map
					drawPhysicalMap(mainBorder, mainBorder, front);
					break;
			}
		}

		private void drawProbabilityMap(int topX, int topY, Graphics g) {
			Font origFont = g.getFont();
			g.setFont(new Font("Arial", Font.PLAIN, 10));
			int cellShift = cellSize;
			for (int y = 0; y < Map.HEIGHT; y++) {
				for (int x = 0; x < Map.WIDTH; x++) {
					final int baseX = cellShift * x + topX;
					final int baseY = cellShift * y + topY;
					final int spacing = cellSize / 5;
					drawProbabilityBar(new Rectangle(baseX, baseY, cellSize, spacing), x, y, Field.UNKNOWN, g);
					drawProbabilityBar(new Rectangle(baseX, baseY + spacing, cellSize, spacing), x, y, Field.WALL, g);
					drawProbabilityBar(new Rectangle(baseX, baseY + spacing * 2, cellSize, spacing), x, y, Field.SPACE, g);
					drawProbabilityBar(new Rectangle(baseX, baseY + spacing * 3, cellSize, spacing), x, y, Field.SPACE_GREEN, g);
					drawProbabilityBar(new Rectangle(baseX, baseY + spacing * 4, cellSize, spacing), x, y, Field.SPACE_RED, g);
					g.setColor(Color.BLACK);
					g.drawRect(baseX, baseY, cellSize, cellSize);
				}
			}
			g.setFont(origFont);
		}

		private void drawProbabilityBar(Rectangle rect, int x, int y, Field field, Graphics g) {
			int index;
			for (index = 0; index < Map.Unit.FIELD_KEY.length; index++) {
				if (Map.Unit.FIELD_KEY[index] == field)
					break;
			}
			double probability = Map.getMap().get(x, y).getProbabilities()[index];
			double percent = probability / Map.Unit.MAX_PROBABILITY;
			/*float intensity = Math.round(percent);

			if (intensity > 1f)
				intensity = 1f;
			if (intensity < 0f)
				intensity = 0f;*/
			float intensity = 1f;
			float hue = 0f;
			float value = 1f;
			//String name = "N/A";
			switch (field) {
				case UNKNOWN:
					//name = "?";
					hue = 300f / 360f;
					value = 1f;
					break;
				case WALL:
					//name = "W";
					hue = 240f / 360f;
					value = 1f;
					break;
				case SPACE:
					//name = "S";
					hue = 60f / 360f;
					value = 1f;
					break;
				case SPACE_RED:
					//name = "R";
					hue = 0f;
					value = 1f;
					break;
				case SPACE_GREEN:
					//name = "G";
					hue = 120f / 360f;
					value = 1f;
					break;
			}
			Color c = Color.getHSBColor(hue, intensity, value);
			g.setColor(Color.WHITE);
			g.fillRect(rect.x, rect.y, rect.width, rect.height);
			g.setColor(c);
			g.fillRect(rect.x, rect.y, (int) Math.round(rect.width * percent), rect.height);
			g.setColor(Color.BLACK);
			g.drawRect(rect.x, rect.y, rect.width, rect.height);
			/*
			String prob_string = new DecimalFormat("#.0###",
					DecimalFormatSymbols.getInstance(Locale.ENGLISH)).format(probability);
			int height = g.getFontMetrics().getHeight();
			int str_y = rect.y + height - 4;
			g.drawString(name + ":" + prob_string, rect.x + 2, str_y);
			*/
		}

		private void drawImage(Rectangle rect, Image src, Graphics g) {
			g.drawImage(src, rect.x, rect.y, rect.x + rect.width, rect.y + rect.height,
					0, 0, src.getWidth(null), src.getHeight(null), null);
		}

		private void drawRobot(int topX, int topY, Graphics g, int alpha) {
			int x = (int) Math.round(reality.getRobotX() /
					((double) Settings.INT.FIELD_SIZE.getValue() * Map.WIDTH) * mapWidth + topX); // center
			int y = (int) Math.round(reality.getRobotY() /
					((double) Settings.INT.FIELD_SIZE.getValue() * Map.HEIGHT) * mapHeight + topY); // center
			int w = (int) Math.round((double) reality.getRobotWidth() /
					Settings.INT.FIELD_SIZE.getValue() * tileSize);
			int h = (int) Math.round((double) reality.getRobotLength() /
					Settings.INT.FIELD_SIZE.getValue() * tileSize);
			Graphics2D robo = (Graphics2D) g.create();
			robo.rotate(Math.toRadians(-reality.getRobotAzimuth()), x, y);
			Color robotColor = reality.getMagnet() == VirtualReality.Magnet.Down ? Color.YELLOW : Color.ORANGE;
			robotColor = new Color(robotColor.getRed(), robotColor.getGreen(), robotColor.getBlue(), alpha);
			robo.setColor(robotColor);
			robo.fillRect(x, y, w, h);
			robo.setColor(new Color(0, 0, 0, alpha));
			robo.drawRect(x, y, w, h);
			robo.setFont(new Font(robo.getFont().getName(), Font.BOLD, 25));
			robo.drawString("↑", x + robo.getFontMetrics().charWidth('↑') / 2,
					y + robo.getFontMetrics().getHeight());
		}

		private void drawPhysicalMap(int topX, int topY, Graphics g) {
			Rectangle rect;
			int cellShift = cellSize;
			for (int y = 0; y < Map.HEIGHT; y++) {
				for (int x = 0; x < Map.WIDTH; x++) {
					rect = new Rectangle(cellShift * x + topX, cellShift * y + topY,
							cellSize, cellSize);
					Color color = Color.WHITE;
					Color brightColor = Color.WHITE;
					switch (reality.getRealMap()[x][y]) {
						case WALL:
							color = new Color(225, 225, 225);
							brightColor = new Color(color.getRed() - 20, color.getGreen() - 20, color.getBlue() - 20);
							break;
						case LIGHT_RED:
							color = Color.RED;
							brightColor = new Color(color.getRed(), color.getGreen() + 100, color.getBlue() + 100);
							break;
						case LIGHT_GREEN:
							color = Color.GREEN;
							brightColor = new Color(color.getRed() + 100, color.getGreen(), color.getBlue() + 100);
							break;
						case LIGHT_OFF:
							color = Color.DARK_GRAY;
							brightColor = new Color(color.getRed() + 20, color.getGreen() + 20, color.getBlue() + 20);
							break;
					}
					if (mode == Mode.EDIT && rect.contains(mouseX, mouseY)) {
						g.setColor(brightColor);
					} else {
						g.setColor(color);
					}
					g.fillRect(rect.x, rect.y, rect.width, rect.height);
					g.setColor(Color.BLACK);
					g.drawRect(rect.x, rect.y, rect.width, rect.height);
				}
			}
		}
	}

	private class RadioListener implements ActionListener {
		public RadioListener() {
		}

		@Override
		public void actionPerformed(ActionEvent actionEvent) {
			if (actionEvent.getActionCommand().equals("test")) {
				setMode(Mode.TEST);
			} else if (actionEvent.getActionCommand().equals("edit")) {
				setMode(Mode.EDIT);
			}
		}
	}

	private class ProbabilityInfoPopupMenu extends JPopupMenu {
		int x, y;
		double[] probabilities;

		public ProbabilityInfoPopupMenu(int x, int y) {
			super("Info at [" + x + "," + y + "]");
			this.x = x;
			this.y = y;
			probabilities = Map.getMap().get(x, y).getProbabilities();
			// TODO see Map.Unit#FIELD_KEY
			register("Unknown", 0);
			register("Wall", 1);
			register("Space", 4);
			register("Green light", 2);
			register("Red light", 3);
		}

		private void register(String name, int prob_index) {
			JMenuItem item = new JMenuItem();
			double probability = probabilities[prob_index]; // TODO link with real program
			item.setText(name + ": " + probability);
			item.setToolTipText("Calculated probability of a " + name.toLowerCase() + " in this place.");
			item.setEnabled(false);
			add(item);
		}
	}

	private class EditPopupMenu extends JPopupMenu implements ActionListener {
		private int x;
		private int y;

		public EditPopupMenu(int x, int y) {
			super("Set field [" + (x + 1) + "," + (y + 1) + "]");
			this.x = x;
			this.y = y;
			register("Wall", "wall", KeyEvent.VK_W);
			register("Red light", "red", KeyEvent.VK_R);
			register("Green light", "green", KeyEvent.VK_G);
			register("Off light", "off", KeyEvent.VK_O);
		}

		private void register(String name, String action, int mnemonic) {
			JMenuItem item = new JMenuItem(name);
			item.setActionCommand(action);
			item.setMnemonic(mnemonic);
			item.addActionListener(this);
			item.setToolTipText("Set this field to " + name.toLowerCase());
			add(item);
		}

		@Override
		public void actionPerformed(ActionEvent actionEvent) {
			RealField set;
			switch (actionEvent.getActionCommand()) {
				case "red":
					set = RealField.LIGHT_RED;
					break;
				case "green":
					set = RealField.LIGHT_GREEN;
					break;
				case "off":
					set = RealField.LIGHT_OFF;
					break;
				case "wall":
					set = RealField.WALL;
					break;
				default:
					return;
			}
			setTile(x, y, set);
		}
	}

	private class HeartbeatListener extends Thread {
		VirtualReality reality;
		Component redrawTarget;

		public HeartbeatListener(VirtualReality reality, Component redrawTarget) {
			super("Control structure event listener");
			this.reality = reality;
			this.redrawTarget = redrawTarget;
			setDaemon(true);
		}

		@Override
		public void run() {
			while (!this.isInterrupted()) {
				try {
					synchronized (reality.getHeartbeat()) {
						reality.getHeartbeat().wait();
					}
					SwingUtilities.invokeAndWait(new Runnable() {
						@Override
						public void run() {
							redrawTarget.repaint();
						}
					});
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	private class RunListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent actionEvent) {
			if (stopped) {
				startProgram();
			} else if (reality.isPaused()) {
				reality.setPause(false);
				synchronized (reality.getPause_lock()) {
					reality.getPause_lock().notifyAll();
				}
			}
		}
	}

	private class PauseListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent actionEvent) {
			if (!(stopped || reality.isPaused())) {
				reality.setPause(true);
			}
		}
	}

	private class StopListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent actionEvent) {
			stopProgram();
		}
	}

	private class MapSaver implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent actionEvent) {
			StringBuilder str = new StringBuilder();
			for (int y = 0; y < Map.HEIGHT; y++) {
				for (int x = 0; x < Map.WIDTH; x++) {
					char append = '?';
					switch (reality.getRealMap()[x][y]) {
						case LIGHT_GREEN:
							append = 'G';
							break;
						case LIGHT_RED:
							append = 'R';
							break;
						case LIGHT_OFF:
							append = '_';
							break;
						case WALL:
							append = 'X';
							break;
					}
					str.append(append);
				}
				str.append('\n');
			}
			JFileChooser fc = new JFileChooser();
			fc.setDialogTitle("Save map");
			int result = fc.showSaveDialog(MainForm.this);
			File f = fc.getSelectedFile();
			if (result != JFileChooser.APPROVE_OPTION) {
				return;
			}
			CharBuffer buf = CharBuffer.wrap(str.toString());
			ByteBuffer byteBuf = StandardCharsets.UTF_8.encode(buf);
			byteBuf.rewind();
			try (FileChannel chan = FileChannel.open(f.toPath(),
					StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING)) {
				chan.write(byteBuf);
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	}

	private class MapLoader implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent actionEvent) {
			stopProgram();
			FileChannel chan = null;
			try {
				JFileChooser fc = new JFileChooser();
				fc.setDialogTitle("Load map");
				int result;
				File f;
				do {
					result = fc.showOpenDialog(MainForm.this);
					f = fc.getSelectedFile();
				} while (result == JFileChooser.APPROVE_OPTION && !f.exists());
				if (result != JFileChooser.APPROVE_OPTION) {
					return;
				}
				chan = FileChannel.open(f.toPath(), StandardOpenOption.READ);
				ByteBuffer byteBuf = ByteBuffer.allocate((int) chan.size());
				chan.read(byteBuf);
				byteBuf.rewind();
				CharBuffer buf = StandardCharsets.UTF_8.decode(byteBuf);
				char[] chars = buf.toString().toCharArray();
				int i = 0;
				for (int y = 0; y < Map.HEIGHT; y++) {
					for (int x = 0; x < Map.WIDTH; x++) {
						char ch;
						do {
							ch = chars[i++];
						} while (ch != '_' && ch != 'X' && ch != 'G' && ch != 'R');
						RealField field = RealField.LIGHT_OFF;
						switch (ch) {
							case '_':
								field = RealField.LIGHT_OFF;
								break;
							case 'X':
								field = RealField.WALL;
								break;
							case 'G':
								field = RealField.LIGHT_GREEN;
								break;
							case 'R':
								field = RealField.LIGHT_RED;
								break;
						}
						reality.getRealMap()[x][y] = field;
					}
				}
				synchronized (reality.getHeartbeat()) {
					reality.getHeartbeat().notifyAll();
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (chan != null) {
					try {
						chan.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	private class DelayUpdater implements ChangeListener {
		@Override
		public void stateChanged(ChangeEvent e) {
			int value = (int) spinner1.getValue();
			if (value < MIN_DELAY) {
				value = MIN_DELAY;
				spinner1.setValue(value);
			}
			if (value > MAX_DELAY) {
				value = MAX_DELAY;
				spinner1.setValue(value);
			}
			reality.setMove_delayPerMm(value);
			reality.setRotate_delayPerDeg(value * 3);
		}
	}
}
