package main;

import lejos.hardware.*;
import lejos.hardware.lcd.TextLCD;
import main.robot.RealRobotics;
import main.robot.ai.AI;

import java.io.File;
import java.io.IOException;

/**
 * Main class for running program on EV3
 *
 * @author Matej Kripner <kripnermatej@gmail.com>
 * @author Jakub Vaněk {@literal vanek.jakub4@seznam.cz}
 * @version 1.0
 */
public class Main {
	/**
	 * Path to configuration file.
	 * Taken from cfgtool.
	 */
	public static final String CONFIG_FILE = "/home/lejos/robot.cfg";
	/**
	 * Button LED light controller.
	 */
	private static LED led;
	/**
	 * EV3 Display.
	 */
	private static TextLCD lcd;

	/**
	 * Main function.
	 *
	 * @param args ignored
	 * @throws IOException Thrown when an error happened during loading of the configuration file.
	 */
	public static void main(String[] args) throws IOException {
		// get brick instance
		Brick local = BrickFinder.getDefault();
		// prepare for loading
		led = local.getLED();
		led.setPattern(2);
		lcd = local.getTextLCD();
		lcd.setAutoRefresh(false);
		lcd.drawString("Loading...", 0, 0);
		lcd.refresh();
		// load subsystems
		Settings.update(new File(CONFIG_FILE));
		RealRobotics realRobotics = new RealRobotics();
		Robotics.setRobotics(realRobotics);
		AI program = new AI();
		// prepare for gyro calibration and do it
		preGyroScreen();
		if (!checkButtons()) return;
		gyroScreen();
		Sound.playTone(400, 500);
		realRobotics.calibrateGyro();
		Sound.playTone(500, 100);
		// show pre-start screen
		preStartScreen();
		if (!checkButtons()) return;
		lcd.clear();
		lcd.refresh();
		led.setPattern(1);
		// run the program
		program.run();
		// clear the instruction cache
		// DANGER
		realRobotics.flush();
		// END OF DANGER
	}

	/**
	 * Draw gyro pre-calibration screen.
	 */
	private static void preGyroScreen() {
		led.setPattern(3);
		beep();
		lcd.clear(0);
		lcd.drawString("Done.", 0, 0);
		lcd.drawString("ENTER -> gyro", 0, 1);
		lcd.drawString("ESCAPE -> exit", 0, 2);
		lcd.refresh();
	}

	/**
	 * Draw gyro calibration screen.
	 */
	private static void gyroScreen() {
		led.setPattern(2);
		lcd.clear(0);
		lcd.clear(1);
		lcd.clear(2);
		lcd.drawString("Calibrating...", 0, 0);
		lcd.refresh();
	}

	/**
	 * Draw pre-start screen.
	 */
	private static void preStartScreen() {
		led.setPattern(1);
		beep();
		lcd.clear(0);
		lcd.drawString("Done.", 0, 0);
		lcd.drawString("ENTER -> start", 0, 1);
		lcd.drawString("ESCAPE -> exit", 0, 2);
		lcd.refresh();
	}

	/**
	 * Wait for button press.
	 *
	 * @return {@code true} if user presses ENTER, {@code false} if user presses ESCAPE.
	 */
	private static boolean checkButtons() {
		while (true) {
			int button = Button.waitForAnyPress();
			if ((button & Button.ID_ESCAPE) != 0)
				return false;
			else if ((button & Button.ID_ENTER) != 0)
				return true;
		}
	}

	/**
	 * Asynchronous beep.
	 */
	private static void beep() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				Sound.beep();
			}
		}).start();
	}
}
