package main;

import main.robot.RealRobotics;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Configuration manager
 *
 * @author Matej Kripner <kripnermatej@gmail.com>
 * @author Jakub Vaněk {@literal vanek.jakub4@seznam.cz}
 * @version 1.1
 */
public final class Settings {
	/**
	 * Properties instance for loading from/saving to file.
	 */
	private static Properties properties;

	/**
	 * Update the specified configuration file with new values.
	 *
	 * @param file File to update
	 * @throws IOException When file error occurs.
	 */
	public static void update(File file) throws IOException {
		// load contents
		properties = new Properties();
		try (FileInputStream stream = new FileInputStream(file)) {
			properties.load(stream);
		}
		// try to load each key-value pair into it's place
		for (FLOAT constant : FLOAT.values()) {
			tryLoadOne(constant);
		}
		for (STRING constant : STRING.values()) {
			tryLoadOne(constant);
		}
		for (INT constant : INT.values()) {
			tryLoadOne(constant);
		}
		for (BOOLEAN constant : BOOLEAN.values()) {
			tryLoadOne(constant);
		}
		// save back the file (with both old and new entries)
		save(file);
	}

	/**
	 * Try to load selected key-value pair value from default properties instance.
	 *
	 * @param value Key-value pair to load into.
	 */
	private static void tryLoadOne(Value value) {
		try {
			value.load(properties);
		} catch (Exception e) {
			System.out.println("[" + value.getKey() + "] is new");
		}
	}

	/**
	 * Load settings from file
	 *
	 * @param file Path to file
	 * @throws IOException           Thrown if something goes wrong with file open.
	 * @throws IllegalStateException Thrown if the file doesn't contain some of predefined keys.
	 */
	public static void load(File file) throws IOException {
		// load contents
		properties = new Properties();
		try (FileInputStream stream = new FileInputStream(file)) {
			properties.load(stream);
		}
		// load each key-value pair into it's place - throws exception when the file doesn't contain given key.
		for (FLOAT constant : FLOAT.values()) {
			constant.load(properties);
		}
		for (STRING constant : STRING.values()) {
			constant.load(properties);
		}
		for (INT constant : INT.values()) {
			constant.load(properties);
		}
		for (BOOLEAN constant : BOOLEAN.values()) {
			constant.load(properties);
		}
	}

	/**
	 * Save settings to file
	 *
	 * @param file Path to file
	 * @throws IOException Thrown if something goes wrong with file open.
	 */
	public static void save(File file) throws IOException {
		// load each key-value pair value into properties
		for (FLOAT constant : FLOAT.values()) {
			constant.save(properties);
		}
		for (STRING constant : STRING.values()) {
			constant.save(properties);
		}
		for (INT constant : INT.values()) {
			constant.save(properties);
		}
		for (BOOLEAN constant : BOOLEAN.values()) {
			constant.save(properties);
		}
		// write properties back to original file
		try (FileOutputStream stream = new FileOutputStream(file)) {
			properties.store(stream, "Saved configuration from the main program");
		}
	}

	/**
	 * Float constant enumeration
	 */
	public enum FLOAT implements Value<Float> {
		/**
		 * The distance between wheels. As this is partially specific to ground surface, this value is intended to be fine-tuned.
		 * Unit: mm
		 */
		TRACK_WIDTH("TRACK_WIDTH", 104f),
		/**
		 * The diameter of the left wheel.
		 * Unit: mm
		 */
		LEFT_WHEEL_DIAMETER("LEFT_WHEEL_DIAMETER", 54.95f),
		/**
		 * The diameter of the right wheel.
		 * Unit: mm
		 */
		RIGHT_WHEEL_DIAMETER("RIGHT_WHEEL_DIAMETER", 54.95f),
		/**
		 * Proportional constant used for turning corrections. Value choice depends on the implementation.
		 * Unit: -
		 */
		TURN_KP("TURN_KP", 0.9f),
		/**
		 * Maximal acceptable error during the turn of the robot.
		 * Unit: degree
		 */
		TURN_MAX_ERROR("TURN_MAX_ERROR", 3),
		/**
		 * Proportional constant used for straight move corrections. Value choice depends on the implementation.
		 * Current implementation uses this for changing the speed of robot's wheels.
		 * Unit: -
		 */
		REGULATOR_KP("REGULATOR_KP", 5f), // mm/s per mm
		/**
		 * Brightness limit used for detecting red light.
		 * Unit: 0-100 % (check EV3 HDK for the scale)
		 */
		COLOR_LIMIT("COLOR_LIMIT", 70f), // in percentage
		/**
		 * How much should we trust the ultrasonic sensor.
		 * Unit: 0-1
		 */
		SONIC_PRECISION("SONIC_PRECISION", 0.9f),
		/**
		 * How much should we trust the touch sensor.
		 * Unit: 0-1
		 */
		TOUCH_PRECISION("TOUCH_PRECISION", 0.97f),
		/**
		 * Minimal speed of the motors.
		 * Unit: 0-1
		 */
		REGULATOR_MIN_SPEED_PERCENT("REGULATOR_MIN_SPEED_PERCENT", 0.25f);

		private final String key;
		private float value;

		FLOAT(String key, float defaultValue) {
			this.key = key;
			this.value = defaultValue;
		}

		public Float getValue() {
			return value;
		}

		public void setValue(Float f) {
			this.value = f;
		}

		public String getKey() {
			return key;
		}

		public void load(Properties list) {
			this.value = Float.parseFloat(list.getProperty(this.key));
		}

		public void save(Properties list) {
			list.setProperty(this.key, Float.toString(this.value));
		}
	}

	/**
	 * Integer constant enumeration
	 */
	public enum INT implements Value<Integer> {
		/**
		 * Speed of magnet servo.
		 * Unit: degree/s
		 */
		MAGNET_MOTOR_SPEED("MAGNET_MOTOR_SPEED", 360),
		/**
		 * Acceleration of magnet servo.
		 * Unit: degree/s^2
		 */
		MAGNET_MOTOR_ACCEL("MAGNET_MOTOR_ACCEL", 2880),
		/**
		 * Position of the magnet servo when magnet is in its down position.
		 * Unit: degree
		 */
		MAGNET_DOWN("MAGNET_DOWN", 0),
		/**
		 * Position of the magnet servo when magnet is in its up position.
		 * Unit: degree
		 */
		MAGNET_UP("MAGNET_UP", 90),
		/**
		 * How long should the robot wait before it stops its motors when it detects wall using touch sensor.
		 * Unit: ms
		 */
		ALIGN_WAIT("ALIGN_WAIT", 0),
		/**
		 * Size of the field.
		 * Unit: mm
		 */
		FIELD_SIZE("FIELD_SIZE", 280),
		/**
		 * Speed of the robot during straight line move.
		 * Unit: mm/s
		 */
		FORWARD_SPEED("FORWARD_SPEED", 280),
		/**
		 * Acceleration of the robot during straight line move.
		 * Unit: mm/s^2
		 */
		FORWARD_ACCEL("FORWARD_ACCEL", 420),
		/**
		 * Angular speed of <b>the robot</b> during rotation.
		 * Unit: degree/s
		 */
		ROTATE_SPEED("ROTATE_SPEED", 180),
		/**
		 * Angular acceleration of <b>the robot</b> during rotation.
		 * Unit: degrees/s^2
		 */
		ROTATE_ACCEL("ROTATE_ACCEL", 180),
		/**
		 * The width of the robot.
		 * Unit: mm
		 */
		ROBOT_WIDTH("ROBOT_WIDTH", 152),
		/**
		 * Size of the light / Hall sensor.
		 * Unit: mm
		 */
		LIGHT_SIZE("LIGHT_SIZE", 50),
		/**
		 * How far can the ultrasonic sensor see.
		 * Unit: blocks
		 */
		MAX_SONIC_BLOCKS("MAX_ULTRASONIC_BLOCKS", 4),
		/**
		 * Which color measurement strategy will the robot use.
		 *
		 * @see RealRobotics#LIGHT_AMBIENT Ambient mode. Light will be only received.
		 * @see RealRobotics#LIGHT_REFLECT Red reflection mode. Red light will be emitted.
		 * @see RealRobotics#LIGHT_RGB RGB reflection mode. Red, green and blue light will be periodically emitted.
		 */
		COLOR_MODE("COLOR_MODE", RealRobotics.LIGHT_AMBIENT),
		/**
		 * How long should the regulator sleep between its iterations.
		 * Unit: ms
		 */
		REGULATOR_SLEEP_MILLIS("REGULATOR_SLEEP_MILLIS", 10),
		/**
		 * Limit for ignoring the tachometer difference between wheels.
		 * Unit: mm
		 */
		REGULATOR_NOOP_LIMIT("REGULATOR_NOOP_LIMIT", 1),
		/**
		 * How long should the regulator after its launch ignore the movement of motors.
		 * Unit: mm
		 */
		REGULATOR_HEATUP_LIMIT("REGULATOR_HEATUP_LIMIT", 20),
		/**
		 * Distance from the wheels to the front side of the robot.
		 * Unit: mm
		 */
		WHEELS_TO_HEAD("WHEELS_TO_HEAD", 70),
		/**
		 * Distance from the wheels to the back side of the robot.
		 * Unit: mm
		 */
		WHEELS_TO_BACK("WHEELS_TO_BACK", 105),
		/**
		 * Distance from the wheels to the middle of the magnet, when the magnet is in its up position.
		 * Unit: mm
		 */
		WHEELS_TO_MAGNET_UP("WHEELS_TO_MAGNET_UP", 160),
		/**
		 * Distance from the wheels to the middle of the magnet, when the magnet is in its down position.
		 * Unit: mm
		 */
		WHEELS_TO_MAGNET_DOWN("WHEELS_TO_MAGNET_DOWN", 117),
		/**
		 * Distance from the wheels to the ultrasonic sensor.
		 * Unit: mm
		 */
		WHEELS_TO_US("WHEELS_TO_US", 57),
		/**
		 * How thick is the magnet part.
		 * Unit: mm
		 */
		MAGNET_THICKNESS("MAGNET_THICKNESS", 15),
		/**
		 * What is the limit for turn error correction during straight line mode move.
		 * Unit: degree
		 */
		TRAVEL_MAX_TURN_ERROR("TRAVEL_MAX_BUG", 35),
		/**
		 * What is the maximal offset for updating expected gyro.
		 * Unit: degree
		 */
		REGULATOR_MAX_FIX_OFFSET("REGULATOR_MAX_FIX_OFFSET", 25);
		private final String key;
		private int value;

		INT(String key, int defaultval) {
			this.key = key;
			this.value = defaultval;
		}

		public Integer getValue() {
			return value;
		}

		public void setValue(Integer f) {
			this.value = f;
		}

		public String getKey() {
			return key;
		}

		public void load(Properties list) {
			this.value = Integer.parseInt(list.getProperty(this.key));
		}

		public void save(Properties list) {
			list.setProperty(this.key, Integer.toString(this.value));
		}
	}

	/**
	 * Boolean constant enumeration
	 */
	public enum BOOLEAN implements Value<Boolean> {
		/**
		 * Whether robot should try to find a red light when it doesn't see it.
		 */
		CRAZY_LIGHT_FINDER("CRAZY_LIGHT", false);

		private final String key;
		private boolean value;

		BOOLEAN(String key, boolean defaultval) {
			this.key = key;
			this.value = defaultval;
		}

		public Boolean getValue() {
			return value;
		}

		public void setValue(Boolean f) {
			this.value = f;
		}

		public String getKey() {
			return key;
		}

		public void load(Properties list) {
			this.value = Boolean.parseBoolean(list.getProperty(this.key));
		}

		public void save(Properties list) {
			list.setProperty(this.key, Boolean.toString(this.value));
		}
	}

	/**
	 * String constant enumeration
	 */
	public enum STRING implements Value<String> {
		/**
		 * Name of the port connected to gyroscopic sensor.
		 * Values: S1,S2,S3,S4
		 */
		SENSOR_GYRO_PORT("GYRO_PORT", "S1"),
		/**
		 * Name of the port connected to the color sensor.
		 * Values: S1,S2,S3,S4
		 */
		SENSOR_COLOR_PORT("COLOR_PORT", "S4"),
		/**
		 * Name of the port connected to the ultrasonic sensor.
		 * Values: S1,S2,S3,S4
		 */
		SENSOR_SONIC_PORT("SONIC_PORT", "S2"),
		/**
		 * Name of the port connected to the touch sensor.
		 * Values: S1,S2,S3,S4
		 */
		SENSOR_TOUCH_PORT("TOUCH_PORT", "S3"),
		/**
		 * Name of the port connected to the left motor.
		 * Values: A,B,C,D
		 */
		LEFT_MOTOR_PORT("LEFT_MOTOR_PORT", "A"),
		/**
		 * Name of the port connected to the right motor.
		 * Values: A,B,C,D
		 */
		RIGHT_MOTOR_PORT("RIGHT_MOTOR_PORT", "B"),
		/**
		 * Name of the port connected to the magnet servo.
		 * Values: A,B,C,D
		 */
		MAGNET_MOTOR_PORT("MAGNET_MOTOR_PORT", "C");

		private final String key;
		private String value;

		STRING(String key, String defaultval) {
			this.key = key;
			this.value = defaultval;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String f) {
			this.value = f;
		}

		public String getKey() {
			return key;
		}

		public void load(Properties list) {
			String prop = list.getProperty(this.key);
			if (prop == null)
				throw new IllegalStateException("null string");
			this.value = prop;
		}

		public void save(Properties list) {
			list.setProperty(this.key, this.value);
		}
	}

	/**
	 * Value enumeration
	 *
	 * @param <T> Type of the value
	 */
	public interface Value<T> {
		/**
		 * Get the value.
		 *
		 * @return Value.
		 */
		T getValue();

		/**
		 * Set the value.
		 *
		 * @param value Value.
		 */
		void setValue(T value);

		/**
		 * Get the key.
		 *
		 * @return Locally unique key.
		 */
		String getKey();

		/**
		 * Save the value into specified Properties class instance.
		 *
		 * @param list Properties class instance.
		 */
		void save(Properties list);

		/**
		 * Load the value from specified Properties class instance.
		 *
		 * @param list Properties class instance.
		 */
		void load(Properties list);
	}
}
