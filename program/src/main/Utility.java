package main;

import lejos.robotics.SampleProvider;

/**
 * Utility class (common functions)
 *
 * @author Jakub Vaněk {@literal vanek.jakub4@seznam.cz}
 * @author Matej Kripner <kripnermatej@gmail.com>
 * @version 0.1
 */
public final class Utility {
	/**
	 * Fetch single sample and get it's index-th element.
	 *
	 * @param provider SampleProvider to sample.
	 * @param index    Index of required element.
	 * @return Element.
	 */
	public static float getSampleElement(SampleProvider provider, int index) {
		return getFullSample(provider)[index];
	}

	/**
	 * Fetch full sample.
	 *
	 * @param provider SampleProvider to sample.
	 * @return Full sample.
	 */
	public static float[] getFullSample(SampleProvider provider) {
		float[] sample = new float[provider.sampleSize()];
		provider.fetchSample(sample, 0);
		return sample;
	}
}
