package main.robot.ai.map;

/**
 * @author Matej Kripner <kripnermatej@gmail.com>
 * @version 1.0
 */
public enum Field {
	UNKNOWN(1800, false, false),
	WALL(-1000, false, true),
	SPACE(2000, true, false),
	SPACE_RED(3000, true, false),
	SPACE_GREEN(0, true, false);

	private final int attractiveness;
	private boolean isSpace;
	private boolean isWall;

	Field(int attractiveness, boolean isSpace, boolean isWall) {
		this.attractiveness = attractiveness;
		this.isSpace = isSpace;
		this.isWall = isWall;
	}

	public int getAttractiveness() {
		return attractiveness;
	}

	public boolean isSpace() {
		return isSpace;
	}

	public boolean isWall() {
		return isWall;
	}
}
