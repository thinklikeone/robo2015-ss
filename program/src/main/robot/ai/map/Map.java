package main.robot.ai.map;

import main.robot.ai.Robot;

import java.io.*;

/**
 * Space abstraction - map of the playing field.
 *
 * @author Matej Kripner <kripnermatej@gmail.com>
 * @author Jakub Vaněk {@literal vanek.jakub4@seznam.cz}
 * @version 0.2
 */
public class Map implements Serializable {
	private static final String FILE_NAME = "map.ser";

	public static final int WIDTH = 9;
	public static final int HEIGHT = 6;
	private static final double MAX_BAD_THINGS = 5;
	private static Map instance;
	/**
	 * Jagged array. First indexer is the X coordinate, second is the Y coordinate.
	 * Implementation note: the [0;0] from the interface view is [1;1] in real.
	 */
	private Unit[][] map;
	private transient double badThings = 0;

	/**
	 * Initialize and reset the map.
	 */
	private Map() {
		reset();
	}

	/**
	 * Get the map singleton instance.
	 *
	 * @return Map instance.
	 */
	public static Map getMap() {
		return instance == null ? instance = new Map() : instance;
	}

	/**
	 * Get the unit on specified place.
	 *
	 * @param x X coordinate of the place.
	 * @param y Y coordinate of the place.
	 * @return Probability unit.
	 */
	public Unit get(int x, int y) {
		if (!checkBounds(x, y)) return new Unit(true, 0, 1, 0, 0, 0);
		return map[x + 1][y + 1];
	}

	public Unit get(Point point) {
		return get(point.getX(), point.getY());
	}

	/**
	 * Set the unit on specified place.
	 *
	 * @param unit Probability unit.
	 * @param x    X coordinate of the place.
	 * @param y    Y coordinate of the place.
	 */
	public void set(Unit unit, int x, int y) {
		if (!checkBounds(x, y))
			throw new IllegalArgumentException("X or Y coordinate is out of range");
		map[x + 1][y + 1] = unit;
	}

	/**
	 * Please call this when the values stored in map seem to differ from the actual reality.
	 */
	public void somethingWrong(double probability) {
		if ((badThings + probability) >= MAX_BAD_THINGS) {
			badThings = 0;
			panic();
		}
	}

	private void panic() {
		System.out.println("panic!!");
		int max = Math.max(HEIGHT, WIDTH);
		map = generateMap(max * 2 + 1, max * 2 + 1);
		Robot.getRobot().setLocation(new Point(max + 1, max + 1));
	}

	/**
	 * Check map boundaries
	 *
	 * @param x X coordinate
	 * @param y Y coordinate
	 * @return If X and Y coordinates are in range 0-WIDTH, respective 0-HEIGHT.
	 */
	public boolean checkBounds(int x, int y) {
		return x >= -1 && x < map.length - 1 && y >= -1 && y < map[0].length - 1;
	}

	public boolean checkBounds(Point point) {
		return checkBounds(point.getX(), point.getY());
	}

	public void save() throws IOException {
		try (ObjectOutputStream w = new ObjectOutputStream(new FileOutputStream(new File(FILE_NAME)))) {
			w.writeObject(this);
		}

	}

	public void load() throws IOException {
		Map read;
		try (ObjectInputStream r = new ObjectInputStream(new FileInputStream(new File(FILE_NAME)))) {
			read = (Map) r.readObject();
		} catch (ClassNotFoundException e) {
			System.out.println("The map file was damaged");
			e.printStackTrace();
			return;
		}
		if (read.map.length == map.length && read.map[0].length == map[0].length) {
			for (int x = 0; x < read.map.length; x++) {
				for (int y = 0; y < read.map[0].length; y++) {
					map[x][y].setProbabilities(read.map[x][y].getProbabilities(), 0.5);
				}
			}
		}
	}

	/**
	 * Fill the map with unknown probability level high.
	 */
	public void reset() {
		map = generateMap(WIDTH + 2, HEIGHT + 2);
		// add start blocks
		set(new Unit(0, 1, 0, 0, 0), 3, 3);
		set(new Unit(0, 0, 1, 0, 0), 4, 3);
		set(new Unit(0, 1, 0, 0, 0), 5, 3);

		badThings = 0;
	}

	private Unit[][] generateMap(int width, int height) {
		Unit[][] map = new Unit[width][height];
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				if (y == 0 || x == 0 || x == width - 1 || y == height - 1)
					map[x][y] = new Unit(true, 0, 1, 0, 0, 0); // edges
				else map[x][y] = new Unit();
			}
		}
		return map;
	}

	/**
	 * Probability unit
	 *
	 * @author Jakub Vaněk {@literal vanek.jakub4@seznam.cz}
	 * @author Matej Kripner <kripnermatej@gmail.com>
	 * @version 0.1
	 */
	public static class Unit implements Serializable {
		public static final double MAX_PROBABILITY = 1f;
		/**
		 * Probability keying array.
		 */
		public static final Field[] FIELD_KEY = {Field.UNKNOWN, Field.WALL, Field.SPACE_GREEN, Field.SPACE_RED, Field.SPACE};
		private double[] probabilities;

		private transient volatile boolean changed = true;
		private transient double highestProbability;
		private transient Field withHighestProbability;

		private boolean outOfBounds;

		/**
		 * Default constructor - unknown probability set high.
		 */
		Unit() {
			this(Unit.MAX_PROBABILITY, 0, 0, 0, 0);
		}

		/**
		 * Preferred constructor - copy specified probability levels into new instance of Unit.
		 *
		 * @param unknownProbability Probability of unknown field.
		 * @param wallProbability    Probability of wall.
		 * @param greenProbability   Probability of space with green light.
		 * @param redProbability     Probability of space with red light.
		 */
		public Unit(double unknownProbability, double wallProbability, double spaceProbability, double greenProbability, double redProbability) {
			this(false, unknownProbability, wallProbability, greenProbability, redProbability, spaceProbability);
		}

		/**
		 * Special constructor - allow to set the outOfBounds flag
		 *
		 * @param outOfBounds        the out-of-bounds flag
		 * @param unknownProbability Probability of unknown field.
		 * @param wallProbability    Probability of wall.
		 * @param greenProbability   Probability of space with green light.
		 * @param redProbability     Probability of space with red light.
		 */
		public Unit(boolean outOfBounds, double unknownProbability, double wallProbability, double spaceProbability, double greenProbability, double redProbability) {
			this.probabilities = new double[] {unknownProbability, wallProbability, greenProbability, redProbability, spaceProbability};
			this.outOfBounds = outOfBounds;
		}

		/**
		 * Get the field with highest probability.
		 *
		 * @return Field with highest probability value.
		 */
		public synchronized Field getWithHighestProbability() {
			updateProbabilities();
			return withHighestProbability;
		}

		/**
		 * Get the highest probability.
		 *
		 * @return Highest probability value.
		 */
		public synchronized double getHighestProbability() {
			updateProbabilities();
			return highestProbability;
		}

		private void updateProbabilities() {
			if (!changed) return;
			double max = 0;
			int maxIndex = 0;
			for (int i = 0; i < probabilities.length; i++) {
				if (max < probabilities[i]) {
					max = probabilities[i];
					maxIndex = i;
				}
			}
			withHighestProbability = FIELD_KEY[maxIndex];
			highestProbability = max;
			changed = false;
		}

		/**
		 * Get the probability of unknown field.
		 *
		 * @return Probability of unknown field.
		 */
		public double getUnknownProbability() {
			return probabilities[0];
		}

		/**
		 * Set the probability of unknown field.
		 *
		 * @param probability Probability of unknown field.
		 */
		public void setUnknownProbability(double probability) {
			probabilities[0] = probability;
		}

		/**
		 * Get the probability of wall field.
		 *
		 * @return Probability of wall field.
		 */
		public double getWallProbability() {
			return probabilities[1];
		}

		/**
		 * Set the probability of wall field.
		 *
		 * @param probability Probability of wall field.
		 */
		public void setWallProbability(double probability) {
			probabilities[1] = probability;
		}

		/**
		 * Get the probability of space with green light field.
		 *
		 * @return Probability of space with green light field.
		 */
		public double getGreenProbability() {
			return probabilities[2];
		}

		/**
		 * Set the probability of space with green light field.
		 *
		 * @param probability Probability of space with green light field.
		 */
		public void setGreenProbability(double probability) {
			probabilities[2] = probability;
		}

		/**
		 * Get the probability of space.
		 *
		 * @return Probability of space.
		 */
		public double getSpaceProbability() {
			return probabilities[4];
		}

		/**
		 * Set the probability of space.
		 *
		 * @param probability Probability of space.
		 */
		public void setSpaceProbability(double probability) {
			probabilities[4] = probability;
		}

		/**
		 * Get the probability of space with red light field.
		 *
		 * @return Probability of space with red light field.
		 */
		public double getRedProbability() {
			return probabilities[3];
		}

		/**
		 * Set the probability of space with red light field.
		 *
		 * @param probability Probability of space with red light field.
		 */
		public void setRedProbability(double probability) {
			probabilities[3] = probability;
		}

		/**
		 * Get probability array with indices keyed according to {@link Unit#FIELD_KEY}
		 *
		 * @return Probability array.
		 */
		public double[] getProbabilities() {
			return probabilities;
		}

		/**
		 * Set probability array with indices keyed according to {@link Unit#FIELD_KEY}
		 *
		 * @param probabilities Probability array.
		 * @param probability
		 * @throws IllegalArgumentException If the length is not equal to {@link Map.Unit#probabilities}.length.
		 */
		public synchronized void setProbabilities(double[] probabilities, double probability) {
			if (probabilities.length != this.probabilities.length)
				throw new IllegalArgumentException("Wrong count!");
			if (probability < 0) probability = 0;
			if (probability > 1) probability = 1;
			for (int i = 0; i < this.probabilities.length; i++) {
				this.probabilities[i] = (probability * probabilities[i] + (1 - probability) * this.probabilities[i]);
			}
			changed = true;
		}

		/**
		 * Set the probability of specified field to 1 and the other's probability to 0,
		 *
		 * @param f Field for which to set probability 1.
		 */
		public synchronized void setField(Field f) {
			for (int i = 0; i < FIELD_KEY.length; i++) {
				if (f == FIELD_KEY[i]) {
					probabilities[i] = Unit.MAX_PROBABILITY;
				} else {
					probabilities[i] = 0;
				}
			}
			withHighestProbability = f;
			highestProbability = 1;
			changed = false;
		}


		public boolean isResponsible() {
			return getHighestProbability() >= 0.8;
		}

		public double getAttractiveness() {
			double attractiveness = 0;
			for (int i = 0; i < this.probabilities.length; i++) {
				attractiveness += probabilities[i] * FIELD_KEY[i].getAttractiveness();
			}
			return attractiveness;
		}

		public void checkWall(double probability) {
			if (isResponsible() && probability >= 0.5 && getWithHighestProbability() != Field.WALL && getWithHighestProbability() != Field.UNKNOWN) {
				Map.getMap().somethingWrong((getHighestProbability() + probability) / 2);
			}
		}

		public void checkSpace(double probability) {
			if (isResponsible() && probability >= 0.5 && getWithHighestProbability() == Field.WALL) {
				Map.getMap().somethingWrong((getHighestProbability() + probability) / 2);
			}
		}

		public boolean isOutOfBounds() {
			return outOfBounds;
		}
	}
}
