package main.robot;

import lejos.hardware.port.Port;
import lejos.hardware.sensor.EV3ColorSensor;
import lejos.hardware.sensor.EV3GyroSensor;
import lejos.hardware.sensor.EV3TouchSensor;
import lejos.hardware.sensor.EV3UltrasonicSensor;
import lejos.robotics.Color;
import lejos.robotics.SampleProvider;
import main.Settings;
import main.Utility;

import static main.Robotics.Light;

/**
 * Class for accessing robot sensors.
 *
 * @author Jakub Vaněk {@literal <vanek.jakub4@seznam.cz>}
 * @version 0.1
 */
class Sensors {
	private static Sensors instance = null;
	private final SampleProvider sonic;
	private final SampleProvider gyro;
	private final EV3GyroSensor gyroSensor;
	private final SampleProvider light;
	private final SampleProvider touch;

	/**
	 * Create new instance of Sensors class.
	 *
	 * @param sonic Port connected to the ultrasonic sensor.
	 * @param gyro  Port connected to the gyroscopic sensor.
	 * @param light Port connected to the color sensor.
	 * @param touch Port connected to the touch sensor.
	 */
	public Sensors(Port sonic, Port gyro, Port light, Port touch) {
		EV3UltrasonicSensor sonicSensor = new EV3UltrasonicSensor(sonic);
		sonicSensor.enable();
		this.sonic = sonicSensor.getDistanceMode();
		this.touch = new EV3TouchSensor(touch).getTouchMode();
		this.gyroSensor = new EV3GyroSensor(gyro);
		this.gyro = gyroSensor.getAngleMode();
		EV3ColorSensor lightSensor = new EV3ColorSensor(light);
		switch (Settings.INT.COLOR_MODE.getValue()) {
			case RealRobotics.LIGHT_REFLECT:
				lightSensor.setFloodlight(Color.RED);
				this.light = lightSensor.getRedMode();
				break;
			default:
			case RealRobotics.LIGHT_AMBIENT:
				lightSensor.setFloodlight(Color.BLUE);
				this.light = lightSensor.getAmbientMode();
				break;
			case RealRobotics.LIGHT_RGB:
				lightSensor.setFloodlight(Color.WHITE);
				this.light = lightSensor.getRGBMode();
				break;
		}
	}

	/**
	 * Get static instance
	 *
	 * @return Sensors instance if it was initialized, null otherwise.
	 */
	public static Sensors getInstance() {
		return instance;
	}

	/**
	 * Set static instance
	 *
	 * @param instance Initialized Sensors class
	 */
	public static void setInstance(Sensors instance) {
		if (instance == null)
			throw new IllegalStateException("Reset is not allowed");
		if (Sensors.instance == null)
			Sensors.instance = instance;
		else
			throw new IllegalStateException("Already set");
	}

	/**
	 * Measure robot's azimuth using gyroscopic sensor.
	 *
	 * @return Angle in degrees, CCW is positive.
	 */
	public float measureAngle() {
		return Utility.getSampleElement(gyro, 0);
	}

	/**
	 * Calibrate the gyroscope. Note: this operation takes a long time.
	 */
	public void calibrateGyro() {
		gyroSensor.reset();
	}

	/**
	 * Measure distance from the wall using ultrasonic sensor.
	 *
	 * @return Distance in millimeters.
	 */
	public float measureDistance() {
		return Utility.getSampleElement(sonic, 0) * 1000f; // 1000 mm in 1 m
	}

	/**
	 * Check if the robot is touching the wall.
	 *
	 * @return True if we are against the wall.
	 */
	public boolean measureTouch() {
		return Utility.getSampleElement(touch, 0) == 1f;
	}

	/**
	 * Measure light under the robot using color sensor.
	 *
	 * @return {@link Light#RED} if measured light exceeds limit, {@link Light#OTHER} if not.
	 * @throws IllegalStateException if RGB mode is configured. This might change, so look into the source for details.
	 */
	public Light measureLight() {
		switch (Settings.INT.COLOR_MODE.getValue()) {
			default:
			case RealRobotics.LIGHT_AMBIENT:
			case RealRobotics.LIGHT_REFLECT:
				float scalar = Utility.getSampleElement(light, 0) * 100;
				if (scalar > Settings.FLOAT.COLOR_LIMIT.getValue())
					return Light.RED;
				else
					return Light.OTHER;
			case RealRobotics.LIGHT_RGB:
				throw new IllegalStateException("RGB mode is not implemented");
		}
	}
}
